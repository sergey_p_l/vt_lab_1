package com.bsuir.smartphonesShop.Controller.command.Exception;

public class CommandException extends Exception {
    public CommandException(String message, Exception e) {
        super(message, e);
    }

    public CommandException(String message) {
        super(message);
    }

}
