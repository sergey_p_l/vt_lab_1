package com.bsuir.smartphonesShop.Controller.command;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.Entity.IdClass;
import com.bsuir.smartphonesShop.Service.BaseService;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.LinkedList;

public abstract class BaseCommand<TService extends BaseService, TObj extends IdClass> {

    /**
     * @return service instance for working with current model.
     */
    protected abstract TService getService();

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    public String execute(ArrayList<String> args) throws CommandException {
        switch (args.get(0)) {
            case ("add"):
                args.remove(0);
                return add(args);
            case ("delete"):
                args.remove(0);
                return delete(args);
            case ("update"):
                args.remove(0);
                return update(args);
            case ("search"):
                args.remove(0);
                return search(args);
            case ("get"):
                args.remove(0);
                return getString(args.get(0));
            case ("show"):
                args.remove(0);
                return show(args);
            case ("load"):
                return load(args);
        }
        throw new CommandException(String.format("command '%s' not found!", args.get(0)));

    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    public abstract String load(ArrayList<String> args) throws CommandException;

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    protected abstract String objToString(TObj obj);

    /**
     * @param args is list with given users command to system.
     * @return string version of all showing objects.
     */
    private String show(ArrayList<String> args) {
        TService service = getService();
        LinkedList list = service.showObjects(args.get(0), args.get(1));
        StringBuilder response = new StringBuilder();
        for (var obj : list) {
            response.append(obj.toString()).append("\n");
        }
        return response.toString();
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    private String add(ArrayList<String> args) throws CommandException {
        try {
            TService service = getService();
            if (service.addObject(args))
                return getString(args.get(args.size() - 1));
            else
                return null;
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    private String delete(ArrayList<String> args) throws CommandException {
        try {
            TService chargerService = getService();
            if (chargerService.deleteObject(Integer.parseInt(args.get(0))))
                return "Deleting successfull!";
            else
                return null;
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    public abstract String search(ArrayList<String> args);

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    public abstract String update(ArrayList<String> args) throws CommandException;

    /**
     * @param id of necessary object.
     * @return object by id.
     * @throws CommandException throws if received ServiceException.
     */
    public TObj get(String id) throws CommandException {
        TService service = getService();
        TObj obj = (TObj) service.getObject(Integer.parseInt(id));
        if (obj != null)
            return obj;
        else {
            throw new CommandException("Object not found!");
        }
    }

    /**
     * @param id of object, which is needed to translate to string values.
     * @return string version with all fields of object by id.
     * @throws CommandException throws if received ServiceException.
     */
    protected String getString(String id) throws CommandException {
        try {
            TObj obj = get(id);
            return objToString(obj);
        } catch (CommandException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }
}
