package com.bsuir.smartphonesShop.Controller.command.implementation;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Manufacturer;
import com.bsuir.smartphonesShop.Service.ManufacturerService;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;
import com.bsuir.smartphonesShop.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class ManufacturerCommand extends BaseCommand<ManufacturerService, Manufacturer> {

    /**
     * @return service instance for working with current model.
     */
    @Override
    protected ManufacturerService getService() {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        return serviceFactory.getManufacturerService();
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String update(ArrayList<String> args) throws CommandException {
        try {
            ManufacturerService service = getService();

            Manufacturer obj = get(args.get(0));
            ArrayList<String> fields = new ArrayList<>();
            if (args.contains("country"))
                fields.add(args.get(args.indexOf("country") + 1));
            else
                fields.add(obj.getCountry());

            if (args.contains("fullName"))
                fields.add(args.get(args.indexOf("fullName") + 1));
            else
                fields.add(obj.getFullName());

            service.updateObject(args.get(0), fields);
            return getString(args.get(0));
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String load(ArrayList<String> args) throws CommandException {
        try {
            ManufacturerService service = getService();
            return service.loadObjects() ? "Manufacturer DB was loaded successfully." : "Loading Manufacturer DB was aborted";
        } catch (DAOException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    @Override
    public String search(ArrayList<String> args) {
        ManufacturerService service = getService();
        HashMap<String, String> query = new HashMap<>();
        if (args.contains("country"))
            query.put("country", args.get(args.indexOf("country") + 1));
        if (args.contains("fullName"))
            query.put("fullName", args.get(args.indexOf("fullName") + 1));
        if (args.contains("ID"))
            query.put("ID", args.get(args.indexOf("ID") + 1));
        LinkedList<Manufacturer> objects = service.searchObjects(query);
        StringBuilder result = new StringBuilder();
        for (var elem : objects)
            result.append(elem.toString()).append("\n");
        return result.toString();
    }

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    @Override
    protected String objToString(Manufacturer obj) {
        return String.format("'%s' '%s' '%s'", obj.getCountry(), obj.getFullName(), String.valueOf(obj.getID()));
    }
}
