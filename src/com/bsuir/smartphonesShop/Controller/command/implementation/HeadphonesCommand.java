package com.bsuir.smartphonesShop.Controller.command.implementation;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Headphones;
import com.bsuir.smartphonesShop.Service.HeadphonesService;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;
import com.bsuir.smartphonesShop.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class HeadphonesCommand extends BaseCommand<HeadphonesService, Headphones> {

    /**
     * @return service instance for working with current model.
     */
    @Override
    protected HeadphonesService getService() {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        return serviceFactory.getHeadphonesService();
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String update(ArrayList<String> args) throws CommandException {
        try {
            HeadphonesService service = getService();

            Headphones obj = get(args.get(0));
            ArrayList<String> fields = new ArrayList<>();
            if (args.contains("type"))
                fields.add(args.get(args.indexOf("type") + 1));
            else
                fields.add(obj.getType());

            if (args.contains("typeOfConnector"))
                fields.add(args.get(args.indexOf("typeOfConnector") + 1));
            else
                fields.add(obj.getTypeOfConnector());

            if (args.contains("manufacturer"))
                fields.add(args.get(args.indexOf("manufacturer") + 1));
            else
                fields.add(String.valueOf(obj.getManufacturer().getID()));

            if (args.contains("price"))
                fields.add(args.get(args.indexOf("price") + 1));
            else
                fields.add(String.valueOf(obj.getPrice()));

            service.updateObject(args.get(0), fields);
            return getString(args.get(0));
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String load(ArrayList<String> args) throws CommandException {
        try {
            HeadphonesService service = getService();
            return service.loadObjects() ? "Headphones DB was loaded successfully." : "Loading Headphones DB was aborted";
        } catch (DAOException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    @Override
    public String search(ArrayList<String> args) {
        HeadphonesService service = getService();
        HashMap<String, String> query = new HashMap<>();
        if (args.contains("type"))
            query.put("type", args.get(args.indexOf("type") + 1));
        if (args.contains("typeOfConnector"))
            query.put("typeOfConnector", args.get(args.indexOf("typeOfConnector") + 1));
        if (args.contains("manufacturer"))
            query.put("manufacturer", args.get(args.indexOf("manufacturer") + 1));
        if (args.contains("price"))
            query.put("price", args.get(args.indexOf("price") + 1));
        if (args.contains("ID"))
            query.put("ID", args.get(args.indexOf("ID") + 1));
        LinkedList<Headphones> objects = service.searchObjects(query);
        StringBuilder result = new StringBuilder();
        for (var elem : objects)
            result.append(elem.toString()).append("\n");
        return result.toString();
    }

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    @Override
    protected String objToString(Headphones obj) {
        return String.format("'%s' '%s' '%s' '%s' '%s'", obj.getType(), obj.getTypeOfConnector(),
                String.valueOf(obj.getManufacturer().getID()),
                String.valueOf(obj.getPrice()), String.valueOf(obj.getID()));
    }
}
