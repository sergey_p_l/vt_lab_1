package com.bsuir.smartphonesShop.Controller.command.implementation;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Smartphone;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;
import com.bsuir.smartphonesShop.Service.SmartphoneService;
import com.bsuir.smartphonesShop.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class SmartphoneCommand extends BaseCommand<SmartphoneService, Smartphone> {

    /**
     * @return service instance for working with current model.
     */
    @Override
    protected SmartphoneService getService() {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        return serviceFactory.getSmartphoneService();
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String load(ArrayList<String> args) throws CommandException {
        try {
            SmartphoneService service = getService();
            return service.loadObjects() ? "Smartphone DB was loaded successfully." : "Loading Smartphone DB was aborted";
        } catch (DAOException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String update(ArrayList<String> args) throws CommandException {
        try {
            SmartphoneService service = getService();

            Smartphone obj = get(args.get(0));
            ArrayList<String> fields = new ArrayList<>();
            if (args.contains("smartphoneOS"))
                fields.add(args.get(args.indexOf("smartphoneOS") + 1));
            else
                fields.add(String.valueOf(obj.getSmartphoneOS()));

            if (args.contains("batteryVolume"))
                fields.add(args.get(args.indexOf("batteryVolume") + 1));
            else
                fields.add(String.valueOf(obj.getBatteryVolume()));

            if (args.contains("versionOS"))
                fields.add(args.get(args.indexOf("versionOS") + 1));
            else
                fields.add(String.valueOf(obj.getVersionOS()));

            if (args.contains("fullName"))
                fields.add(args.get(args.indexOf("fullName") + 1));
            else
                fields.add(String.valueOf(obj.getFullName()));

            if (args.contains("manufacturer"))
                fields.add(args.get(args.indexOf("manufacturer") + 1));
            else
                fields.add(String.valueOf(obj.getManufacturer().getID()));

            if (args.contains("typeOfHeadphonesConnector"))
                fields.add(args.get(args.indexOf("typeOfHeadphonesConnector") + 1));
            else
                fields.add(String.valueOf(obj.getTypeOfHeadphonesConnector()));

            if (args.contains("typeOfChargerConnector"))
                fields.add(args.get(args.indexOf("typeOfChargerConnector") + 1));
            else
                fields.add(String.valueOf(obj.getTypeOfChargerConnector()));

            if (args.contains("price"))
                fields.add(args.get(args.indexOf("price") + 1));
            else
                fields.add(String.valueOf(obj.getPrice()));

            service.updateObject(args.get(0), fields);
            return getString(args.get(0));
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    @Override
    public String search(ArrayList<String> args) {
        SmartphoneService service = getService();
        HashMap<String, String> query = new HashMap<>();
        if (args.contains("smartphoneOS"))
            query.put("smartphoneOS", args.get(args.indexOf("smartphoneOS") + 1));
        if (args.contains("batteryVolume"))
            query.put("batteryVolume", args.get(args.indexOf("batteryVolume") + 1));
        if (args.contains("versionOS"))
            query.put("versionOS", args.get(args.indexOf("versionOS") + 1));
        if (args.contains("fullName"))
            query.put("fullName", args.get(args.indexOf("fullName") + 1));
        if (args.contains("manufacturer"))
            query.put("manufacturer", args.get(args.indexOf("manufacturer") + 1));
        if (args.contains("typeOfHeadphonesConnector"))
            query.put("typeOfHeadphonesConnector", args.get(args.indexOf("typeOfHeadphonesConnector") + 1));
        if (args.contains("typeOfChargerConnector"))
            query.put("typeOfChargerConnector", args.get(args.indexOf("typeOfChargerConnector") + 1));
        if (args.contains("price"))
            query.put("price", args.get(args.indexOf("price") + 1));
        if (args.contains("ID"))
            query.put("ID", args.get(args.indexOf("ID") + 1));
        LinkedList<Smartphone> objects = service.searchObjects(query);
        StringBuilder result = new StringBuilder();
        for (var elem : objects)
            result.append(elem.toString()).append("\n");
        return result.toString();
    }

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    @Override
    protected String objToString(Smartphone obj) {
        return String.format("'%s' '%s' '%s' '%s' '%s' '%s' '%s' '%s' '%s'", obj.getSmartphoneOS(),
                String.valueOf(obj.getBatteryVolume()), obj.getVersionOS(), obj.getFullName(),
                String.valueOf(obj.getManufacturer().getID()), obj.getTypeOfHeadphonesConnector(),
                obj.getTypeOfChargerConnector(), String.valueOf(obj.getPrice()), String.valueOf(obj.getID()));
    }
}

