package com.bsuir.smartphonesShop.Controller.command.implementation;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.User;
import com.bsuir.smartphonesShop.Service.UserService;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;
import com.bsuir.smartphonesShop.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class UserCommand extends BaseCommand<UserService, User> {

    /**
     * @return service instance for working with current model.
     */
    @Override
    protected UserService getService() {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        return serviceFactory.getUserService();
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String execute(ArrayList<String> args) throws CommandException {
        if (args.get(0).equals("auth")) {
            args.remove(0);
            return auth(args);
        }
        return super.execute(args);
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String load(ArrayList<String> args) throws CommandException {
        try {
            UserService service = getService();
            return service.loadObjects() ? "User DB was loaded successfully." : "Loading User DB was aborted";
        } catch (DAOException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String update(ArrayList<String> args) throws CommandException {
        try {
            UserService service = getService();

            User obj = get(args.get(0));
            ArrayList<String> fields = new ArrayList<>();
            if (args.contains("name"))
                fields.add(args.get(args.indexOf("name") + 1));
            else
                fields.add(obj.getName());

            if (args.contains("surname"))
                fields.add(args.get(args.indexOf("surname") + 1));
            else
                fields.add(obj.getSurname());

            if (args.contains("login"))
                fields.add(args.get(args.indexOf("login") + 1));
            else
                fields.add(obj.getLogin());

            if (args.contains("password"))
                fields.add(args.get(args.indexOf("password") + 1));
            else
                fields.add(obj.getPassword());

            if (args.contains("email"))
                fields.add(args.get(args.indexOf("email") + 1));
            else
                fields.add(obj.getEmail());

            if (args.contains("isAdmin"))
                fields.add(args.get(args.indexOf("isAdmin") + 1));
            else
                fields.add((obj.getIsAdmin() ? "True" : "False"));

            service.updateObject(args.get(0), fields);
            return getString(args.get(0));
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with all parameters for user authenticate.
     * @return authenticated user as string version with all field or error message.
     * @throws CommandException throws if received ServiceException.
     */
    private String auth(ArrayList<String> args) throws CommandException {
        UserService service = getService();
        HashMap<String, String> query = new HashMap<>();
        query.put("login", args.get(args.indexOf("login") + 1));
        query.put("password", args.get(args.indexOf("password") + 1));
        ArrayList<User> objects = service.auth(query);
        if (objects.size() == 1)
            return objToString(objects.get(0));
        else
            throw new CommandException("Bad request. Specific User not found.");
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    @Override
    public String search(ArrayList<String> args) {
        UserService service = getService();
        HashMap<String, String> query = new HashMap<>();
        if (args.contains("name"))
            query.put("name", args.get(args.indexOf("name") + 1));
        if (args.contains("surname"))
            query.put("surname", args.get(args.indexOf("surname") + 1));
        if (args.contains("login"))
            query.put("login", args.get(args.indexOf("login") + 1));
        if (args.contains("password"))
            query.put("password", args.get(args.indexOf("password") + 1));
        if (args.contains("email"))
            query.put("email", args.get(args.indexOf("email") + 1));
        if (args.contains("ID"))
            query.put("ID", args.get(args.indexOf("ID") + 1));
        LinkedList<User> objects = service.searchObjects(query);
        StringBuilder result = new StringBuilder();
        for (var elem : objects)
            result.append(elem.toString()).append("\n");
        return result.toString();
    }

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    @Override
    protected String objToString(User obj) {
        return String.format("'%s' '%s' '%s' '%s' '%s' '%s' '%s'", obj.getName(), obj.getSurname(), obj.getLogin(),
                obj.getPassword(), obj.getEmail(), obj.getIsAdmin() ? "True" : "False", String.valueOf(obj.getID()));
    }
}
