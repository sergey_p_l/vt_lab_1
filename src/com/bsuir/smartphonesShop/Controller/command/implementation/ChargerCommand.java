package com.bsuir.smartphonesShop.Controller.command.implementation;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Charger;
import com.bsuir.smartphonesShop.Service.ChargerService;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;
import com.bsuir.smartphonesShop.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class ChargerCommand extends BaseCommand<ChargerService, Charger> {

    /**
     * @return service instance for working with current model.
     */
    @Override
    protected ChargerService getService() {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        return serviceFactory.getChargerService();
    }

    /**
     * @param args is list with given users command to system.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String load(ArrayList<String> args) throws CommandException {
        try {
            ChargerService service = getService();
            return service.loadObjects() ? "Charger DB was loaded successfully." : "Loading Charger DB was aborted";
        } catch (DAOException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with all parameters for updating object.
     * @return result of executing users command as string.
     * @throws CommandException throws if received ServiceException.
     */
    @Override
    public String update(ArrayList<String> args) throws CommandException {
        try {
            ChargerService service = getService();

            Charger obj = get(args.get(0));
            ArrayList<String> fields = new ArrayList<>();
            if (args.contains("typeOfConnector"))
                fields.add(args.get(args.indexOf("typeOfConnector") + 1));
            else
                fields.add(obj.getTypeOfConnector());

            if (args.contains("length"))
                fields.add(args.get(args.indexOf("length") + 1));
            else
                fields.add(String.valueOf(obj.getLength()));

            if (args.contains("color"))
                fields.add(args.get(args.indexOf("color") + 1));
            else
                fields.add(obj.getColor());

            if (args.contains("manufacturer"))
                fields.add(args.get(args.indexOf("manufacturer") + 1));
            else
                fields.add(String.valueOf(obj.getManufacturer().getID()));

            if (args.contains("price"))
                fields.add(args.get(args.indexOf("price") + 1));
            else
                fields.add(String.valueOf(obj.getTypeOfConnector()));

            service.updateObject(args.get(args.indexOf("ID") + 1), fields);
            return getString(args.get(0));
        } catch (ServiceException err) {
            throw new CommandException(err.getMessage(), err);
        }
    }

    /**
     * @param args is list with given users command to system.
     * @return string version of all searching objects.
     */
    @Override
    public String search(ArrayList<String> args) {
        ChargerService service = getService();
        HashMap<String, String> query = new HashMap<>();
        if (args.contains("typeOfConnector"))
            query.put("typeOfConnector", args.get(args.indexOf("typeOfConnector") + 1));
        if (args.contains("length"))
            query.put("length", args.get(args.indexOf("length") + 1));
        if (args.contains("color"))
            query.put("color", args.get(args.indexOf("color") + 1));
        if (args.contains("manufacturer"))
            query.put("manufacturer", args.get(args.indexOf("manufacturer") + 1));
        if (args.contains("price"))
            query.put("price", args.get(args.indexOf("price") + 1));
        if (args.contains("ID"))
            query.put("ID", args.get(args.indexOf("ID") + 1));
        LinkedList<Charger> objects = service.searchObjects(query);
        StringBuilder result = new StringBuilder();
        for (var elem : objects)
            result.append(elem.toString()).append("\n");
        return result.toString();
    }

    /**
     * @param obj is object for convert to string version with all fields.
     * @return result of executing users command as string.
     */
    @Override
    protected String objToString(Charger obj) {
        return String.format("'%s' '%s' '%s' '%s' '%s' '%s'", obj.getTypeOfConnector(),
                String.valueOf(obj.getLength()), obj.getColor(), String.valueOf(obj.getManufacturer().getID()),
                obj.getPrice(), String.valueOf(obj.getID()));
    }
}
