package com.bsuir.smartphonesShop.Controller;

import com.bsuir.smartphonesShop.Controller.command.BaseCommand;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.Controller.command.implementation.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Controller {
    private HashMap<String, BaseCommand> commands = new HashMap<>();

    public Controller() {
        commands.put("charger", new ChargerCommand());
        commands.put("headphones", new HeadphonesCommand());
        commands.put("manufacturer", new ManufacturerCommand());
        commands.put("powerbank", new PowerBankCommand());
        commands.put("smartphone", new SmartphoneCommand());
        commands.put("user", new UserCommand());
    }


    public String execute(String command) throws CommandException {
        ArrayList<String> params = getParamsFromCommand(command);
        if (!params.get(0).equals("load")) {
            BaseCommand exec = commands.get(params.get(0));
            params.remove(0);
            return exec.execute(params);
        } else {
            return String.format("%s\n%s\n%s\n%s\n%s\n%s",
                    (new ManufacturerCommand()).execute(params),
                    (new ChargerCommand()).execute(params),
                    (new HeadphonesCommand()).execute(params),
                    (new PowerBankCommand()).execute(params),
                    (new SmartphoneCommand()).execute(params),
                    (new UserCommand()).execute(params));
        }
    }

    private static ArrayList<String> getParamsFromCommand(String command) {
        ArrayList<String> result = new ArrayList<>();
        result.add(command.split(" ")[0]);
        boolean flag = false;
        StringBuilder word = new StringBuilder();
        for (int i = result.get(0).length(); i < command.length(); i++) {
            if (command.charAt(i) == '\'') {
                flag = !flag;
                if (!flag) {
                    result.add(word.toString());
                    word.delete(0, word.length());
                }
                continue;
            }
            if (flag) {
                word.append(command.charAt(i));
            }
        }
        return result;
    }
}