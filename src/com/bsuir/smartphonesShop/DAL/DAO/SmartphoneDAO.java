package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Smartphone;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class SmartphoneDAO extends ProductDAO<Smartphone> {
    private String dbFileName = "./storage/Smartphones.txt";

    /**
     * @param obj    is writing object in db file
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException
     */
    @Override
    protected void writeItem(Smartphone obj, BufferedWriter writer) throws DAOException {
        try {
            writer.write(obj.getSmartphoneOS());
            writer.newLine();
            writer.write(String.valueOf(obj.getBatteryVolume()));
            writer.newLine();
            writer.write(obj.getVersionOS());
            writer.newLine();
            writer.write(obj.getFullName());
            writer.newLine();
            writer.write(String.valueOf(obj.getManufacturer().getID()));
            writer.newLine();
            writer.write(obj.getTypeOfHeadphonesConnector());
            writer.newLine();
            writer.write(obj.getTypeOfChargerConnector());
            writer.newLine();
            writer.write(String.valueOf(obj.getPrice()));
            writer.newLine();
            writer.write(String.valueOf(obj.getID()));
            writer.newLine();
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(Smartphone obj) throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            writer.write(obj.getSmartphoneOS());
            writer.newLine();
            writer.write(String.valueOf(obj.getBatteryVolume()));
            writer.newLine();
            writer.write(obj.getVersionOS());
            writer.newLine();
            writer.write(obj.getFullName());
            writer.newLine();
            writer.write(String.valueOf(obj.getManufacturer().getID()));
            writer.newLine();
            writer.write(obj.getTypeOfHeadphonesConnector());
            writer.newLine();
            writer.write(obj.getTypeOfChargerConnector());
            writer.newLine();
            writer.write(String.valueOf(obj.getPrice()));
            writer.newLine();
            writer.write(String.valueOf(obj.getID()));
            writer.newLine();
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeAll() throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName))) {
            for (var smartphone : objectsList) {
                writeItem(smartphone, writer);
            }
        } catch (IOException er) {
            throw new DAOException("Error writing file", er);
        }
    }

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    @Override
    protected Smartphone readItem(BufferedReader reader) throws DAOException {
        try {
            String temp;
            ArrayList<String> array = new ArrayList<>();
            while ((temp = reader.readLine()) != null) {
                array.add(temp);
                for (int i = 0; i < 8; i++) {
                    array.add(reader.readLine());
                }
            }
            if (array.isEmpty())
                return null;
            Smartphone obj = new Smartphone();
            itemInit(obj, array);
            return obj;
        } catch (FileNotFoundException ex) {
            throw new DAOException("File not found.", ex);
        } catch (IOException ex) {
            throw new DAOException("Error during updating", ex);
        }
    }

    /**
     * @throws DAOException throws if received error during reading.
     */
    @Override
    public void readAll() throws DAOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {
            objectsList.clear();
            Smartphone temp = readItem(reader);
            while (temp != null) {
                objectsList.add(temp);
                temp = readItem(reader);
            }
        } catch (IOException er) {
            throw new DAOException("Error reading file", er);
        }
    }

    /**
     * @param obj is object for initialization.
     * @param fields is list of string values for object fields.
     */
    @Override
    protected void itemInit(Smartphone obj, ArrayList<String> fields) {
        obj.setSmartphoneOS(fields.get(0));
        obj.setBatteryVolume(Integer.parseInt(fields.get(1)));
        obj.setVersionOS(fields.get(2));
        obj.setFullName(fields.get(3));
        obj.setManufacturer(ManufacturerDAO.getManufacturer(Integer.parseInt(fields.get(4))));
        obj.setTypeOfHeadphonesConnector(fields.get(5));
        obj.setTypeOfChargerConnector(fields.get(6));
        obj.setPrice(Double.parseDouble(fields.get(7)));
        obj.setID(Integer.parseInt(fields.get(8)));
    }

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    @Override
    public boolean addItem(ArrayList<String> fields) throws DAOException {
        Smartphone obj = new Smartphone();
        fields.add(String.valueOf(obj.getID()));
        itemInit(obj, fields);
        if (!objectsList.contains(obj)) {
            objectsList.add(obj);
            writeItem(obj);
        }
        return true;
    }

    /**
     * @param idObj is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    @Override
    public boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException {
        fields.add(String.valueOf(idObj));
        itemInit(getItem(idObj), fields);
        writeAll();
        return true;
    }

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    @Override
    public LinkedList<Smartphone> searchItem(HashMap<String, String> query) {
        LinkedList<Smartphone> result = new LinkedList<>();
        for (var elem : objectsList) {
            for (var pair : query.entrySet()) {
                if (pair.getValue() != null) {
                    if (pair.getKey().equals("smartphoneOS") && elem.getSmartphoneOS().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("batteryVolume") && elem.getBatteryVolume() == Integer.parseInt(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("versionOS") && elem.getVersionOS().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("fullName") && elem.getFullName().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("manufacturer") && elem.getManufacturer().toString().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("typeOfHeadphonesConnector") && elem.getTypeOfHeadphonesConnector().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("typeOfChargerConnector") && elem.getTypeOfChargerConnector().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("price") && elem.getPrice() == Double.parseDouble(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("ID") && elem.getID() == Integer.parseInt(pair.getValue())) {
                        result.add(elem);
                    }
                }
            }
        }
        return result;
    }
}