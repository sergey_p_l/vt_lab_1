package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.IdClass;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public abstract class BaseDAO<T extends IdClass> {
    /**
     * @param obj    is writing object in db file
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException throws if db file not found.
     */
    protected abstract void writeItem(T obj, BufferedWriter writer) throws DAOException;

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    protected abstract void writeItem(T obj) throws DAOException;

    /**
     * @throws DAOException throws if db file not found.
     */
    protected abstract void writeAll() throws DAOException;

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    protected abstract T readItem(BufferedReader reader) throws DAOException;

    /**
     * @throws DAOException throws if received error during reading.
     */
    public abstract void readAll() throws DAOException;

    /**
     * @param obj    is object for initialization.
     * @param fields is list of string values for object fields.
     */
    protected abstract void itemInit(T obj, ArrayList<String> fields);

    /**
     * @param idObj is id of necessary object.
     * @return necessary object by id.
     */
    public abstract T getItem(int idObj);

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    public abstract boolean addItem(ArrayList<String> fields) throws DAOException;

    /**
     * @param idObj is id object must to delete.
     * @return true, if delete successfull, else throw DAOException.
     * @throws DAOException throws if received error during writing or reading db file.
     */
    public abstract boolean deleteItem(int idObj) throws DAOException;

    /**
     * @param idObj  is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    public abstract boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException;

    /**
     * @param start start index of total list objects.
     * @param end   end index of total list objects.
     * @return list with object with id in given range.
     */
    public abstract LinkedList<T> getAll(Integer start, Integer end);

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    public abstract LinkedList<T> searchItem(HashMap<String, String> query);
}
