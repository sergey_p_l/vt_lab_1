package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public abstract class ProductDAO<T extends Product> extends BaseDAO<T> {
    LinkedList<T> objectsList = new LinkedList<>();

    /**
     * @param obj    is writing object in db file.
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected abstract void writeItem(T obj, BufferedWriter writer) throws DAOException;

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected abstract void writeItem(T obj) throws DAOException;

    /**
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected abstract void writeAll() throws DAOException;

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    @Override
    protected abstract T readItem(BufferedReader reader) throws DAOException;

    /**
     * @throws DAOException throws if received error during reading.
     */
    @Override
    public abstract void readAll() throws DAOException;

    /**
     * @param obj    is object for initialization.
     * @param fields is list of string values for object fields.
     */
    @Override
    protected abstract void itemInit(T obj, ArrayList<String> fields);

    /**
     * @param idObj is id of necessary object.
     * @return necessary object of type Manufacturer.
     */
    @Override
    public T getItem(int idObj) {
        return objectsList.stream().filter(x -> x.getID() == idObj).findFirst().orElse(null);
    }

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    @Override
    public abstract boolean addItem(ArrayList<String> fields) throws DAOException;

    /**
     * @param idObj is id object must to delete.
     * @return true, if delete successfull, else throw DAOException.
     * @throws DAOException throws if received error during writing or reading db file.
     */
    @Override
    public boolean deleteItem(int idObj) throws DAOException {
        objectsList.remove(getItem(idObj));
        writeAll();
        readAll();
        return true;
    }

    /**
     * @param idObj  is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    @Override
    public abstract boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException;

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    @Override
    public abstract LinkedList<T> searchItem(HashMap<String, String> query);

    /**
     * @param start start index of total list objects.
     * @param end   end index of total list objects.
     * @return list with object with id in given range.
     */
    @Override
    public LinkedList<T> getAll(Integer start, Integer end) {
        start %= objectsList.size();
        start = max(0, start);
        start = min(objectsList.size() - 1, start);
        end = max(start + 1, end);
        end = min(objectsList.size(), end);
        return new LinkedList<>(objectsList.subList(start, end));
    }
}
