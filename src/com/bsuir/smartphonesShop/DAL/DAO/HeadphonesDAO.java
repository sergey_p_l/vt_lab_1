package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;

import com.bsuir.smartphonesShop.DAL.Entity.Headphones;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class HeadphonesDAO extends ProductDAO<Headphones> {
    private String dbFileName = "./storage/Headphones.txt";

    /**
     * @param obj    is writing object in db file
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(Headphones obj, BufferedWriter writer) throws DAOException {
        try {
            writer.write(obj.getType());
            writer.newLine();
            writer.write(obj.getTypeOfConnector());
            writer.newLine();
            writer.write(String.valueOf(obj.getManufacturer().getID()));
            writer.newLine();
            writer.write(String.valueOf(obj.getPrice()));
            writer.newLine();
            writer.write(String.valueOf(obj.getID()));
            writer.newLine();
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(Headphones obj) throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            writeItem(obj, writer);
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeAll() throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName))) {
            for (var Headphones : objectsList) {
                writeItem(Headphones, writer);
            }
        } catch (IOException er) {
            throw new DAOException("Error writing file", er);
        }
    }

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    @Override
    protected Headphones readItem(BufferedReader reader) throws DAOException {
        try {
            String temp;
            ArrayList<String> array = new ArrayList<>();
            if ((temp = reader.readLine()) != null) {
                array.add(temp);
                for (int i = 0; i < 4; i++) {
                    array.add(reader.readLine());
                }
            }
            if (array.isEmpty())
                return null;
            Headphones obj = new Headphones();
            itemInit(obj, array);
            return obj;
        } catch (FileNotFoundException ex) {
            throw new DAOException("File not found.", ex);
        } catch (IOException ex) {
            throw new DAOException("Error during updating", ex);
        }
    }

    /**
     * @throws DAOException throws if received error during reading.
     */
    @Override
    public void readAll() throws DAOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {
            objectsList.clear();
            Headphones temp = readItem(reader);
            while (temp != null) {
                objectsList.add(temp);
                temp = readItem(reader);
            }
        } catch (IOException er) {
            throw new DAOException("Error reading file", er);
        }
    }

    /**
     * @param obj    is object for initialization.
     * @param fields is list of string values for object fields.
     */
    @Override
    protected void itemInit(Headphones obj, ArrayList<String> fields) {
        obj.setType(fields.get(0));
        obj.setTypeOfConnector(fields.get(1));
        obj.setManufacturer(ManufacturerDAO.getManufacturer(Integer.parseInt(fields.get(2))));
        obj.setPrice(Double.parseDouble(fields.get(3)));
        obj.setID(Integer.parseInt(fields.get(4)));
    }

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    @Override
    public boolean addItem(ArrayList<String> fields) throws DAOException {
        Headphones obj = new Headphones();
        fields.add(String.valueOf(obj.getID()));
        itemInit(obj, fields);
        if (!objectsList.contains(obj)) {
            objectsList.add(obj);
            writeItem(obj);
        }
        return true;
    }

    /**
     * @param idObj  is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    @Override
    public boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException {
        fields.add(String.valueOf(idObj));
        itemInit(getItem(idObj), fields);
        writeAll();
        return true;
    }

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    @Override
    public LinkedList<Headphones> searchItem(HashMap<String, String> query) {
        LinkedList<Headphones> result = new LinkedList<>();
        for (var elem : objectsList) {
            for (var pair : query.entrySet()) {
                if (pair.getValue() != null) {
                    if (pair.getKey().equals("type") && elem.getType().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("typeOfConnector") && elem.getTypeOfConnector().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("manufacturer") && elem.getManufacturer().toString().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("price") && elem.getPrice() == Double.parseDouble(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("ID") && elem.getID() == Integer.parseInt(pair.getValue())) {
                        result.add(elem);
                    }
                }
            }
        }
        return result;
    }
}
