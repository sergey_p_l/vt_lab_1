package com.bsuir.smartphonesShop.DAL.DAO;

public class DAOFactory {
    private static DAOFactory factory = new DAOFactory();

    private final ChargerDAO chargerDAO = new ChargerDAO();
    private final HeadphonesDAO headphonesDAO = new HeadphonesDAO();
    private final ManufacturerDAO manufacturerDAO = new ManufacturerDAO();
    private final PowerBankDAO powerBankDAO = new PowerBankDAO();
    private final SmartphoneDAO smartphoneDAO = new SmartphoneDAO();
    private final UserDAO userDAO = new UserDAO();

    /**
     * @return self instance
     */
    public static DAOFactory getInstance() {
        return factory;
    }

    /**
     * @return DAO instance for work with Chargers
     */
    public ChargerDAO getChargerDAO() {
        return chargerDAO;
    }

    /**
     * @return DAO instance for work with Headphones
     */
    public HeadphonesDAO getHeadphonesDAO() {
        return headphonesDAO;
    }

    /**
     * @return DAO instance for work with Manufacturers
     */
    public ManufacturerDAO getManufacturerDAO() {
        return manufacturerDAO;
    }

    /**
     * @return DAO instance for work with PowerBanks
     */
    public PowerBankDAO getPowerBankDAO() {
        return powerBankDAO;
    }

    /**
     * @return DAO instance for work with Smartphones
     */
    public SmartphoneDAO getSmartphoneDAO() {
        return smartphoneDAO;
    }

    /**
     * @return DAO instance for work with Users
     */
    public UserDAO getUserDAO() {
        return userDAO;
    }
}
