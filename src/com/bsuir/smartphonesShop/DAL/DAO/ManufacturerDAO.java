package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Manufacturer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class ManufacturerDAO extends BaseDAO<Manufacturer> {
    private static LinkedList<Manufacturer> objectsList = new LinkedList<>();
    private String dbFileName = "./storage/Manufacturers.txt";

    /**
     * @param id is id of necessary Manufacturer object.
     * @return necessary object.
     */
    static Manufacturer getManufacturer(int id) {
        return objectsList.stream().filter((x) -> x.getID() == id).findFirst().orElse(null);
    }

    /**
     * @param obj    is writing object in db file
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(Manufacturer obj, BufferedWriter writer) throws DAOException {
        try {
            writer.write(obj.getCountry());
            writer.newLine();
            writer.write(obj.getFullName());
            writer.newLine();
            writer.write(String.valueOf(obj.getID()));
            writer.newLine();
        } catch (IOException e) {
            throw new DAOException(String.format("File '%s' not found.", dbFileName), e);
        }
    }

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(Manufacturer obj) throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            writeItem(obj, writer);
        } catch (IOException e) {
            throw new DAOException(String.format("File '%s' not found.", dbFileName), e);
        }
    }

    /**
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeAll() throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName))) {
            for (var Manufacturer : objectsList) {
                writeItem(Manufacturer, writer);
            }
        } catch (IOException e) {
            throw new DAOException(String.format("File '%s' not found.", dbFileName), e);
        }
    }

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    @Override
    protected Manufacturer readItem(BufferedReader reader) throws DAOException {
        try {
            String temp;
            ArrayList<String> array = new ArrayList<>();
            if ((temp = reader.readLine()) != null) {
                array.add(temp);
                for (int i = 0; i < 2; i++) {
                    array.add(reader.readLine());
                }
            }
            if (array.isEmpty())
                return null;
            Manufacturer obj = new Manufacturer();
            itemInit(obj, array);
            return obj;
        } catch (FileNotFoundException e) {
            throw new DAOException(String.format("File '%s' not found.", dbFileName), e);
        } catch (IOException e) {
            throw new DAOException(String.format("Error during reading file '%s'.", dbFileName), e);
        }
    }

    /**
     * @throws DAOException throws if received error during reading.
     */
    @Override
    public void readAll() throws DAOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {
            objectsList.clear();
            Manufacturer temp = readItem(reader);
            while (temp != null) {
                objectsList.add(temp);
                temp = readItem(reader);
            }
        } catch (IOException e) {
            throw new DAOException(String.format("Error during reading file '%s'.", dbFileName), e);
        }
    }

    /**
     * @param obj    is object for initialization.
     * @param fields is list of string values for object fields.
     */
    @Override
    protected void itemInit(Manufacturer obj, ArrayList<String> fields) {
        obj.setCountry(fields.get(0));
        obj.setFullName(fields.get(1));
        obj.setID(Integer.parseInt(fields.get(2)));
    }

    /**
     * @param idObj is id of necessary object.
     * @return necessary object of type Manufacturer.
     */
    @Override
    public Manufacturer getItem(int idObj) {
        return objectsList.stream().filter(x -> x.getID() == idObj).findFirst().orElse(null);
    }

    /**
     * @param idObj is id object must to delete.
     * @return true, if delete successfull, else throw DAOException.
     * @throws DAOException throws if received error during writing or reading db file.
     */
    @Override
    public boolean deleteItem(int idObj) throws DAOException {
        objectsList.remove(getItem(idObj));
        writeAll();
        readAll();
        return true;
    }

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    @Override
    public boolean addItem(ArrayList<String> fields) throws DAOException {
        Manufacturer obj = new Manufacturer();
        fields.add(String.valueOf(obj.getID()));
        itemInit(obj, fields);
        if (!objectsList.contains(obj)) {
            objectsList.add(obj);
            writeItem(obj);
        }
        return true;
    }

    /**
     * @param idObj  is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    @Override
    public boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException {
        fields.add(String.valueOf(idObj));
        itemInit(getItem(idObj), fields);
        writeAll();
        return true;
    }

    /**
     * @param start start index of total list objects.
     * @param end   end index of total list objects.
     * @return list with object with id in given range.
     */
    @Override
    public LinkedList<Manufacturer> getAll(Integer start, Integer end) {
        start %= objectsList.size();
        start = max(0, start);
        start = min(objectsList.size() - 1, start);
        end = max(start, end);
        end = min(objectsList.size(), end);
        return new LinkedList<>(objectsList.subList(start, end));
    }

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    @Override
    public LinkedList<Manufacturer> searchItem(HashMap<String, String> query) {
        LinkedList<Manufacturer> result = new LinkedList<>();
        for (var elem : objectsList) {
            for (var pair : query.entrySet()) {
                if (pair.getValue() != null) {
                    if (pair.getKey().equals("country") && elem.getCountry().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("fullName") && elem.getFullName().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("ID") && elem.getID() == Integer.parseInt(pair.getValue())) {
                        result.add(elem);
                    }
                }
            }
        }
        return result;
    }
}
