package com.bsuir.smartphonesShop.DAL.DAO;

import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.User;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class UserDAO extends BaseDAO<User> {
    private static LinkedList<User> objectsList = new LinkedList<>();
    private String dbFileName = "./storage/Users.txt";

    /**
     * @param obj    is writing object in db file
     * @param writer is BufferedWriter object, that write objects in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(User obj, BufferedWriter writer) throws DAOException {
        try {
            writer.write(obj.getName());
            writer.newLine();
            writer.write(obj.getSurname());
            writer.newLine();
            writer.write(obj.getLogin());
            writer.newLine();
            writer.write(obj.getPassword());
            writer.newLine();
            writer.write(obj.getEmail());
            writer.newLine();
            writer.write(obj.getIsAdmin() ? "True" : "False");
            writer.newLine();
            writer.write(String.valueOf(obj.getID()));
            writer.newLine();
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @param obj is object writing in db file.
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeItem(User obj) throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            writeItem(obj, writer);
        } catch (IOException ex) {
            throw new DAOException("File not found.", ex);
        }
    }

    /**
     * @throws DAOException throws if db file not found.
     */
    @Override
    protected void writeAll() throws DAOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName))) {
            for (var User : objectsList) {
                writeItem(User, writer);
            }
        } catch (IOException er) {
            throw new DAOException("Error writing file", er);
        }
    }

    /**
     * @param reader is BufferedReader that read objects from file
     * @return first writing object in file.
     * @throws DAOException throws if db file not found or received error during reading.
     */
    @Override
    protected User readItem(BufferedReader reader) throws DAOException {
        try {
            String temp;
            ArrayList<String> array = new ArrayList<>();
            if ((temp = reader.readLine()) != null) {
                array.add(temp);
                for (int i = 0; i < 6; i++) {
                    array.add(reader.readLine());
                }
            }
            if (array.isEmpty())
                return null;
            User obj = new User();
            itemInit(obj, array);
            return obj;
        } catch (FileNotFoundException ex) {
            throw new DAOException("File not found.", ex);
        } catch (IOException ex) {
            throw new DAOException("Error during updating", ex);
        }
    }

    /**
     * @throws DAOException throws if received error during reading.
     */
    @Override
    public void readAll() throws DAOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {
            objectsList.clear();
            User temp = readItem(reader);
            while (temp != null) {
                objectsList.add(temp);
                temp = readItem(reader);
            }
        } catch (IOException er) {
            throw new DAOException("Error reading file", er);
        }
    }

    /**
     * @param obj    is object for initialization.
     * @param fields is list of string values for object fields.
     */
    @Override
    protected void itemInit(User obj, ArrayList<String> fields) {
        obj.setName(fields.get(0));
        obj.setSurname(fields.get(1));
        obj.setLogin(fields.get(2));
        obj.setPassword(fields.get(3));
        obj.setEmail(fields.get(4));
        obj.setIsAdmin(fields.get(5).equals("True"));
        obj.setID(Integer.parseInt(fields.get(6)));
    }

    /**
     * @param idObj is id of necessary object.
     * @return necessary object of type Manufacturer.
     */
    @Override
    public User getItem(int idObj) {
        return objectsList.stream().filter(x -> x.getID() == idObj).findFirst().orElse(null);
    }

    /**
     * @param idObj is id object must to delete.
     * @return true, if delete successfull, else throw DAOException.
     * @throws DAOException throws if received error during writing or reading db file.
     */
    @Override
    public boolean deleteItem(int idObj) throws DAOException {
        objectsList.remove(getItem(idObj));
        writeAll();
        readAll();
        return true;
    }

    /**
     * @param fields is list of string values of new object fields.
     * @return true, if new object created and saved. Else throw DAOException.
     * @throws DAOException throws if received error during writing in db file.
     */
    @Override
    public boolean addItem(ArrayList<String> fields) throws DAOException {
        User obj = new User();
        fields.add("False");
        fields.add(String.valueOf(obj.getID()));
        itemInit(obj, fields);
        if (!objectsList.contains(obj)) {
            objectsList.add(obj);
            writeItem(obj);
        }
        return true;
    }

    /**
     * @param idObj  is id of updating object.
     * @param fields is list of string values of fields updated object.
     * @return true, if updating successfully. Else throw DAOException.
     * @throws DAOException if received error during writing in db file.
     */
    @Override
    public boolean updateItem(int idObj, ArrayList<String> fields) throws DAOException {
        fields.add(String.valueOf(idObj));
        itemInit(getItem(idObj), fields);
        writeAll();
        return true;
    }

    /**
     * @param start start index of total list objects.
     * @param end   end index of total list objects.
     * @return list with object with id in given range.
     */
    @Override
    public LinkedList<User> getAll(Integer start, Integer end) {
        start %= objectsList.size();
        start = max(0, start);
        start = min(objectsList.size() - 1, start);
        end = max(start, end);
        end = min(objectsList.size(), end);
        return (LinkedList<User>) objectsList.subList(start, end);
    }

    /**
     * @param login    is username for auth.
     * @param password is password for auth.
     * @return list with authenticated users.
     */
    public ArrayList<User> authUser(String login, String password) {
        ArrayList<User> result = new ArrayList<>();
        for (var elem : objectsList) {
            if (elem.getLogin().equals(login) && elem.getPassword().equals(password))
                result.add(elem);
        }
        return result;
    }

    /**
     * @param query if dictionary with names of filed and values of query.
     * @return list of suitable objects.
     */
    @Override
    public LinkedList<User> searchItem(HashMap<String, String> query) {
        LinkedList<User> result = new LinkedList<>();
        for (var elem : objectsList) {
            for (var pair : query.entrySet()) {
                if (pair.getValue() != null) {
                    if (pair.getKey().equals("name") && elem.getName().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("surname") && elem.getSurname().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("login") && elem.getLogin().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("password") && elem.getPassword().equals(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("email") && elem.getEmail().contains(pair.getValue())) {
                        result.add(elem);
                    }
                    if (pair.getKey().equals("isAdmin") && elem.getEmail().equals(pair.getValue())) {
                        result.add(elem);
                    }
                }
            }
        }
        return result;
    }
}
