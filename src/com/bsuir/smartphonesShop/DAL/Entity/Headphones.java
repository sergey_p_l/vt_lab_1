package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class Headphones extends Product implements Comparable<Headphones> {
    private String type;
    private String typeOfConnector;
    private Manufacturer manufacturer;

    public Headphones() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeOfConnector() {
        return typeOfConnector;
    }

    public void setTypeOfConnector(String typeOfConnector) {
        this.typeOfConnector = typeOfConnector;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public int compareTo(Headphones headphones) {
        return Comparator.comparing(Headphones::toString)
                .thenComparing(Headphones::getPrice)
                .compare(this, headphones);
    }

    @Override
    public String toString() {
        return String.format("Headphones %s %d", manufacturer, getID());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Headphones headphones = (Headphones) obj;
        return (price == headphones.getPrice())
                && (type != null && type.equals(headphones.getType()))
                && (typeOfConnector != null && typeOfConnector.equals(headphones.getTypeOfConnector()))
                && (manufacturer != null && manufacturer.toString().equals(headphones.getManufacturer().toString()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
