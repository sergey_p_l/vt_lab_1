package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class Manufacturer extends IdClass implements Comparable<Manufacturer> {
    private String country;
    private String fullName;

    public Manufacturer() {
        total_id += 1;
        this.ID = total_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public int compareTo(Manufacturer manufacturer) {
        return Comparator.comparing(Manufacturer::toString)
                .compare(this, manufacturer);
    }

    @Override
    public String toString() {
        return String.format("Manufacturer %s %s", fullName, getID());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Manufacturer manufacturer = (Manufacturer) obj;
        return (country != null && country.equals(manufacturer.getCountry()))
                && (fullName != null && fullName.equals(manufacturer.getFullName()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
