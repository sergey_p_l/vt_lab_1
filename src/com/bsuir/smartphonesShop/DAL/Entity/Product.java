package com.bsuir.smartphonesShop.DAL.Entity;

public class Product extends IdClass {
    double price;

    Product() {
        total_id += 1;
        this.ID = total_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Product product = (Product) obj;
        return (price == product.getPrice());
    }
}
