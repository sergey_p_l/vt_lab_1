package com.bsuir.smartphonesShop.DAL.Entity;

import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(name = "data")
@XmlType(propOrder = {"list"})
@XmlSeeAlso({XMLUser.class, XMLCharger.class, XMLHeadphones.class,
		XMLManufacturer.class, XMLPowerBank.class, XMLSmartphone.class})
public class XMLObjectsList<T> {

	private List<T> list;

	@XmlElement(name = "item")
	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
}
