package com.bsuir.smartphonesShop.DAL.Entity;

import static java.lang.Integer.max;

public class IdClass {
    static int total_id;
    protected int ID;

    public void setID(int ID) {
        total_id = max(total_id, ID);
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }
}
