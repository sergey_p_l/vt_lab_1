package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class PowerBank extends Product implements Comparable<PowerBank> {
    private int volumePower;
    private Manufacturer manufacturer;
    private int countConnections;

    public PowerBank() {}

    public int getVolumePower() {
        return volumePower;
    }

    public void setVolumePower(int volumePower) {
        this.volumePower = volumePower;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCountConnections() {
        return countConnections;
    }

    public void setCountConnections(int countConnections) {
        this.countConnections = countConnections;
    }

    @Override
    public int compareTo(PowerBank powerBank) {
        return Comparator.comparing(PowerBank::toString)
                .thenComparing(PowerBank::getPrice)
                .compare(this, powerBank);
    }

    @Override
    public String toString() {
        return String.format("PowerBank %s %d", manufacturer, getID());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        PowerBank powerBank = (PowerBank) obj;
        return (price == powerBank.getPrice())
                && (volumePower == powerBank.getVolumePower())
                && (manufacturer != null && manufacturer.equals(powerBank.getManufacturer()))
                && (countConnections == powerBank.getCountConnections());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
