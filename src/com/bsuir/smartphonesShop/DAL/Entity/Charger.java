package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class Charger extends Product implements Comparable<Charger> {
    private String typeOfConnector;
    private double length;
    private String color;
    private Manufacturer manufacturer;

    public Charger() {}

    public String getTypeOfConnector() {
        return typeOfConnector;
    }

    public void setTypeOfConnector(String typeOfConnector) {
        this.typeOfConnector = typeOfConnector;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public int compareTo(Charger charger) {
        return Comparator.comparing(Charger::toString)
                .thenComparing(Charger::getPrice)
                .compare(this, charger);
    }

    @Override
    public String toString() {
        return String.format("Charger %s %d", manufacturer, getID());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Charger charger = (Charger) obj;
        return (typeOfConnector != null && typeOfConnector.equals(charger.getTypeOfConnector()))
                && (price == charger.getPrice())
                && (length == charger.getLength())
                && (color != null && color.equals(charger.getColor()))
                && (manufacturer != null && manufacturer.toString().equals(charger.getManufacturer().toString()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
