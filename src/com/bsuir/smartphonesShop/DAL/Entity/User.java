package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class User extends IdClass implements Comparable<User> {
    private String name;
    private String surname;
    private String login;
    private String password;
    private String email;
    private boolean isAdmin;

    public User() {
        total_id += 1;
        this.ID = total_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public int compareTo(User user) {
        return Comparator.comparing(User::toString)
                .compare(this, user);
    }

    @Override
    public String toString() {
        return String.format("User %s %s {%s}", name, surname, login);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        User user = (User) obj;
        return (name != null && name.equals(user.getName()))
                && (surname != null && surname.equals(user.getSurname()))
                && (login != null && login.equals(user.getLogin()))
                && (password != null && password.equals(user.getPassword()))
                && (email != null && email.equals(user.getEmail()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
