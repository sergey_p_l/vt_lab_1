package com.bsuir.smartphonesShop.DAL.Entity;

import java.util.Comparator;

public class Smartphone extends Product implements Comparable<Smartphone> {
    private String smartphoneOS;
    private int batteryVolume;
    private String versionOS;
    private String fullName;
    private Manufacturer manufacturer;
    private String typeOfHeadphonesConnector;
    private String typeOfChargerConnector;

    public Smartphone() {}

    public String getSmartphoneOS() {
        return smartphoneOS;
    }

    public void setSmartphoneOS(String smartphoneOS) {
        this.smartphoneOS = smartphoneOS;
    }

    public int getBatteryVolume() {
        return batteryVolume;
    }

    public void setBatteryVolume(int batteryVolume) {
        this.batteryVolume = batteryVolume;
    }

    public String getVersionOS() {
        return versionOS;
    }

    public void setVersionOS(String versionOS) {
        this.versionOS = versionOS;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTypeOfHeadphonesConnector() {
        return typeOfHeadphonesConnector;
    }

    public void setTypeOfHeadphonesConnector(String typeOfHeadphonesConnector) {
        this.typeOfHeadphonesConnector = typeOfHeadphonesConnector;
    }

    public String getTypeOfChargerConnector() {
        return typeOfChargerConnector;
    }

    public void setTypeOfChargerConnector(String typeOfChargerConnector) {
        this.typeOfChargerConnector = typeOfChargerConnector;
    }

    @Override
    public int compareTo(Smartphone phone) {
        return Comparator.comparing(Smartphone::toString)
                .thenComparing(Smartphone::getPrice)
                .compare(this, phone);
    }

    @Override
    public String toString() {
        return String.format("Smartphone %s %d", fullName, getID());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Smartphone smartphone = (Smartphone) obj;
        return (smartphoneOS != null && smartphoneOS.equals(smartphone.getSmartphoneOS()))
                && (price == smartphone.getPrice())
                && (batteryVolume == smartphone.getBatteryVolume())
                && (versionOS != null && versionOS.equals(smartphone.getVersionOS()))
                && (fullName != null && fullName.equals(smartphone.getFullName()))
                && (manufacturer != null && manufacturer.equals(smartphone.getManufacturer()))
                && (typeOfHeadphonesConnector != null && typeOfHeadphonesConnector.equals(smartphone.getTypeOfHeadphonesConnector()))
                && (typeOfChargerConnector != null && typeOfChargerConnector.equals(smartphone.getTypeOfChargerConnector()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
