package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;

import java.util.*;

public class ChargerView extends BaseView {

    /**
     * Procedure of visual interactive adding object
     */
    private static void addCharger() {
        String request = "";
        System.out.println("######################################");
        System.out.println("#            Add Charger             #");
        System.out.println("######################################");
        System.out.print("# Type Of Connector: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Length: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Color: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Manufacturer ID: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Price: ");
        request += (" '" + scn.nextLine() + "'");

        try {
            controller.execute("charger 'add'" + request);
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive deleting object
     */
    private static void deleteCharger(int objId) {
        try {
            controller.execute(String.format("charger 'delete' '%d'", objId));
        } catch (CommandException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive showing object
     */
    private static void showCharger(int objId) {
        try {
            String info = controller.execute(String.format("charger 'get' '%d'", objId));
            ArrayList<String> infoParams = decoder(info);
            while (true) {
                System.out.println("######################################");
                System.out.println(String.format("# Charger ID: %s", infoParams.get(5)));
                System.out.println(String.format("# Type of connector: %s", infoParams.get(0)));
                System.out.println(String.format("# Length: %s", infoParams.get(1)));
                System.out.println(String.format("# Color: %s", infoParams.get(2)));
                System.out.println(String.format("# Manufacturer: %s", infoParams.get(3)));
                System.out.println(String.format("# Price: %s", infoParams.get(4)));
                System.out.println("######################################");
                System.out.println("# 1 # Update");
                System.out.println("# 2 # Delete");
                System.out.println("# 0 # Exit");
                System.out.print("# Input action: ");
                String action = scn.nextLine();
                switch (action) {
                    case ("1"):
                        updateCharger(objId);
                        info = controller.execute(String.format("charger 'get' '%d'", objId));
                        infoParams = decoder(info);
                        break;
                    case ("2"):
                        deleteCharger(objId);
                        return;
                    case ("0"):
                        return;
                    default:
                        break;
                }
            }
        } catch (CommandException err) {
            System.out.println("\n" + err.getMessage());
        }
    }

    /**
     * Procedure of visual interactive searching object
     *
     * @return string version of found objects
     */
    private static String searchCharger() {
        boolean queryIfFull = false;
        HashMap<String, String> querySet = new HashMap<>();

        while (!queryIfFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to search");
            System.out.println("# 1 # Type of connector");
            System.out.println("# 2 # Length");
            System.out.println("# 3 # Color");
            System.out.println("# 4 # Manufacturer");
            System.out.println("# 5 # Price");
            System.out.println("# 6 # ID");
            System.out.println("# 7 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print(String.format("# Type of connector{%s}: ",
                            (querySet.getOrDefault("typeOfConnector", ""))));
                    query = scn.nextLine();
                    querySet.put("typeOfConnector", query);
                    break;
                case ("2"):
                    System.out.print(String.format("# Length{%s}: ",
                            (querySet.getOrDefault("length", ""))));
                    query = scn.nextLine();
                    querySet.put("length", query);
                    break;
                case ("3"):
                    System.out.print(String.format("# Color{%s}: ",
                            (querySet.getOrDefault("color", ""))));
                    query = scn.nextLine();
                    querySet.put("color", query);
                    break;
                case ("4"):
                    System.out.print(String.format("# Manufacturer{%s}: ",
                            (querySet.getOrDefault("manufacturer", ""))));
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("5"):
                    System.out.print(String.format("# Price{%s}: ",
                            (querySet.getOrDefault("price", ""))));
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("6"):
                    System.out.print(String.format("# ID{%s}: ",
                            (querySet.getOrDefault("ID", ""))));
                    query = scn.nextLine();
                    querySet.put("ID", query);
                    break;
                case ("7"):
                    queryIfFull = true;
                    break;
                case ("0"):
                    return null;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(String.format(" '%s' '%s'", entry.getKey(), entry.getValue()));
        }
        try {
            return controller.execute("charger 'search'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
            return null;
        }
    }

    /**
     * Procedure of visual interactive updating object
     */
    private static void updateCharger(int objId) {
        boolean queryIsFull = false;
        HashMap<String, String> querySet = new HashMap<>();
        ArrayList<String> fields;
        try {
            fields = decoder(controller.execute(String.format("charger 'get' '%d'", objId)));
            querySet.put("typeOfConnector", fields.get(0));
            querySet.put("length", fields.get(1));
            querySet.put("color", fields.get(2));
            querySet.put("manufacturer", fields.get(3));
            querySet.put("price", fields.get(4));
        } catch (CommandException err) {
            System.out.println(err.getMessage());
            return;
        }
        while (!queryIsFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to update");
            System.out.println(String.format("# 1 # Type of connector{%s}: ",
                    (querySet.get("typeOfConnector"))));
            System.out.println(String.format("# 2 # Length{%s}: ",
                    (querySet.get("length"))));
            System.out.println(String.format("# 3 # Color{%s}: ",
                    (querySet.get("color"))));
            System.out.println(String.format("# 4 # Manufacturer{%s}: ",
                    (querySet.get("manufacturer"))));
            System.out.println(String.format("# 5 # Price{%s}: ",
                    (querySet.get("price"))));
            System.out.println("# 6 # Complete");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print("# Type of connector: ");
                    query = scn.nextLine();
                    querySet.put("typeOfConnector", query);
                    break;
                case ("2"):
                    System.out.print("# Length: ");
                    query = scn.nextLine();
                    querySet.put("length", query);
                    break;
                case ("3"):
                    System.out.print("# Color{%s}: ");
                    query = scn.nextLine();
                    querySet.put("color", query);
                    break;
                case ("4"):
                    System.out.print("# Manufacturer: ");
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("5"):
                    System.out.print("# Price: ");
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("6"):
                    queryIsFull = true;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        readyQuery.append(String.format(" '%s'", objId));
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(String.format(" '%s' '%s'", entry.getKey(), entry.getValue()));
        }
        try {
            controller.execute("charger 'update'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
        }
    }

    /**
     * Main entry procedure interactive visual work with objects. Starts with a show list all objects.
     */
    public static void showListChargers() {
        int currentPage = 0;
        String[] objects;
        ArrayList<Integer> idObjects = new ArrayList<>();
        try {
            String response = controller.execute(
                    String.format("charger 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
            objects = response.split("\n");
            for (var elem : objects) {
                if (!elem.isEmpty()) {
                    String[] line = elem.split(" ");
                    idObjects.add(Integer.parseInt(line[line.length - 1]));
                }
            }
        } catch (CommandException e) {
            System.out.println(e.getMessage());
            return;
        }

        while (true) {
            System.out.println("######################################");
            System.out.println("#              Chargers              #");
            System.out.println("######################################");
            if (idObjects.size() != 0) {
                for (int i = 0; i < idObjects.size(); ++i) {
                    System.out.println(String.format("# %d # %s", i + 1, objects[i]));
                }
            } else System.out.println("#              Empty list            #");
            System.out.println("######################################");
            System.out.println("# + # Add new object");
            System.out.println("# @ # Update objects");
            System.out.println("# / # Sort objects");
            System.out.println("# s # Search object");
            System.out.println("# < # Previous page");
            System.out.println("# > # Next page");
            System.out.println("# 0 # Exit");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            switch (action) {
                case ("1"):
                    if (objects.length > 0)
                        showCharger(idObjects.get(0));
                    break;
                case ("2"):
                    if (objects.length > 1)
                        showCharger(idObjects.get(1));
                    break;
                case ("3"):
                    if (objects.length > 2)
                        showCharger(idObjects.get(2));
                    break;
                case ("4"):
                    if (objects.length > 3)
                        showCharger(idObjects.get(3));
                    break;
                case ("5"):
                    if (objects.length > 4)
                        showCharger(idObjects.get(4));
                    break;
                case ("6"):
                    if (objects.length > 5)
                        showCharger(idObjects.get(5));
                    break;
                case ("7"):
                    if (objects.length > 6)
                        showCharger(idObjects.get(6));
                    break;
                case ("8"):
                    if (objects.length > 7)
                        showCharger(idObjects.get(7));
                    break;
                case ("9"):
                    if (objects.length > 8)
                        showCharger(idObjects.get(8));
                    break;
                case ("10"):
                    if (objects.length > 9)
                        showCharger(idObjects.get(9));
                    break;
                case ("+"):
                    addCharger();
                    break;
                case ("@"):
                    try {
                        String response = controller.execute(
                                String.format("charger 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    } catch (CommandException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    break;
                case ("/"):
                    if (objects.length > 0) {
                        naturalSortChargers(objects);
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("s"):
                    String response = searchCharger();
                    if (response != null && !response.isEmpty()) {
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("<"):
                    currentPage -= 1;
                    break;
                case (">"):
                    currentPage += 1;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }
    }
}
