package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class BaseView {
    static Scanner scn = new Scanner(System.in);
    static Controller controller = new Controller();

    /**
     * @param command is big user command.
     * @return all splitted parameters from command line.
     */
    static ArrayList<String> decoder(String command) {
        ArrayList<String> result = new ArrayList<>();
        boolean flag = false;
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < command.length(); i++) {
            if (command.charAt(i) == '\'') {
                flag = !flag;
                if (!flag) {
                    result.add(word.toString());
                    word.delete(0, word.length());
                }
                continue;
            }
            if (flag) {
                word.append(command.charAt(i));
            }
        }
        return result;
    }

    /**
     * @param array is array for natural sorting.
     */
    static void naturalSortChargers(String[] array) {
        Arrays.sort(array, Comparator.naturalOrder());
    }
}
