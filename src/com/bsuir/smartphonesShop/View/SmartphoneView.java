package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SmartphoneView extends BaseView {

    /**
     * Procedure of visual interactive adding object
     */
    private static void addSmartphone() {
        String request = "";
        System.out.println("######################################");
        System.out.println("#           Add Smartphone           #");
        System.out.println("######################################");
        System.out.print("# Smartphone OS: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Battery Volume: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Version OS: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Full Name: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Manufacturer ID: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Type Of Headphones Connector: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Type Of Charger Connector: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Price: ");
        request += (" '" + scn.nextLine() + "'");

        try {
            controller.execute("smartphone 'add'" + request);
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive deleting object
     */
    private static void deleteSmartphone(int objId) {
        try {
            controller.execute(String.format("smartphone 'delete' '%d'", objId));
        } catch (CommandException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive showing object
     */
    private static void showSmartphone(int objId) {
        try {
            String info = controller.execute(String.format("smartphone 'get' '%d'", objId));
            ArrayList<String> infoParams = decoder(info);
            while (true) {
                System.out.println("######################################");
                System.out.println(String.format("# Smartphone ID: %s", infoParams.get(8)));
                System.out.println(String.format("# Smartphone OS: %s", infoParams.get(0)));
                System.out.println(String.format("# Battery volume: %s", infoParams.get(1)));
                System.out.println(String.format("# Version OS: %s", infoParams.get(2)));
                System.out.println(String.format("# Full name: %s", infoParams.get(3)));
                System.out.println(String.format("# Manufacturer: %s", infoParams.get(4)));
                System.out.println(String.format("# Type of headphones connector: %s", infoParams.get(5)));
                System.out.println(String.format("# Type of charger connector: %s", infoParams.get(6)));
                System.out.println(String.format("# Price: %s", infoParams.get(7)));
                System.out.println("######################################");
                System.out.println("# 1 # Update");
                System.out.println("# 2 # Delete");
                System.out.println("# 0 # Exit");
                System.out.print("# Input action: ");
                String action = scn.nextLine();
                switch (action) {
                    case ("1"):
                        updateSmartphone(objId);
                        info = controller.execute(String.format("smartphone 'get' '%d'", objId));
                        infoParams = decoder(info);
                        break;
                    case ("2"):
                        deleteSmartphone(objId);
                        return;
                    case ("0"):
                        return;
                    default:
                        break;
                }
            }
        } catch (CommandException err) {
            System.out.println("\n" + err.getMessage());
        }
    }

    /**
     * Procedure of visual interactive searching object
     *
     * @return string version of found objects
     */
    private static String searchSmartphone() {
        boolean queryIfFull = false;
        HashMap<String, String> querySet = new HashMap<>();

        while (!queryIfFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to search");
            System.out.println("#  1 # Smartphone OS");
            System.out.println("#  2 # Battery volume");
            System.out.println("#  3 # Version OS");
            System.out.println("#  4 # Full name");
            System.out.println("#  5 # Manufacturer");
            System.out.println("#  6 # Type of headphones connector");
            System.out.println("#  7 # Type of charger connector");
            System.out.println("#  8 # Price");
            System.out.println("#  9 # ID");
            System.out.println("# 10 # Search");
            System.out.println("#  0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print(String.format("# Smartphone OS{%s}: ",
                            (querySet.getOrDefault("smartphoneOS", ""))));
                    query = scn.nextLine();
                    querySet.put("smartphoneOS", query);
                    break;
                case ("2"):
                    System.out.print(String.format("# Battery volume{%s}: ",
                            (querySet.getOrDefault("batteryVolume", ""))));
                    query = scn.nextLine();
                    querySet.put("batteryVolume", query);
                    break;
                case ("3"):
                    System.out.print(String.format("# Version OS{%s}: ",
                            (querySet.getOrDefault("versionOS", ""))));
                    query = scn.nextLine();
                    querySet.put("versionOS", query);
                    break;
                case ("4"):
                    System.out.print(String.format("# Full name{%s}: ",
                            (querySet.getOrDefault(" fullName", ""))));
                    query = scn.nextLine();
                    querySet.put("fullName", query);
                    break;
                case ("5"):
                    System.out.print(String.format("# Manufacturer{%s}: ",
                            (querySet.getOrDefault("manufacturer", ""))));
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("6"):
                    System.out.print(String.format("# Type of headphones connector{%s}: ",
                            (querySet.getOrDefault(" typeOfHeadphonesConnector", ""))));
                    query = scn.nextLine();
                    querySet.put("typeOfHeadphonesConnector", query);
                    break;
                case ("7"):
                    System.out.print(String.format("# Type of charger connector{%s}: ",
                            (querySet.getOrDefault("typeOfChargerConnector", ""))));
                    query = scn.nextLine();
                    querySet.put("typeOfChargerConnector", query);
                    break;
                case ("8"):
                    System.out.print(String.format("# Price{%s}: ",
                            (querySet.getOrDefault("price", ""))));
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("9"):
                    System.out.print(String.format("# ID{%s}: ",
                            (querySet.getOrDefault("ID", ""))));
                    query = scn.nextLine();
                    querySet.put("ID", query);
                    break;
                case ("10"):
                    queryIfFull = true;
                    break;
                case ("0"):
                    return null;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(" '").append(entry.getKey()).append("'");
            readyQuery.append(" '").append(entry.getValue()).append("'");
        }
        try {
            return controller.execute("smartphone 'search'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
            return null;
        }
    }

    /**
     * Procedure of visual interactive updating object
     */
    private static void updateSmartphone(int objId) {
        boolean queryIsFull = false;
        HashMap<String, String> querySet = new HashMap<>();
        ArrayList<String> fields;
        try {
            fields = decoder(controller.execute(String.format("smartphone 'get' '%d'", objId)));
            querySet.put("smartphoneOS", fields.get(0));
            querySet.put("batteryVolume", fields.get(1));
            querySet.put("versionOS", fields.get(2));
            querySet.put("fullName", fields.get(3));
            querySet.put("manufacturer", fields.get(4));
            querySet.put("typeOfHeadphonesConnector", fields.get(5));
            querySet.put("typeOfChargerConnector", fields.get(6));
            querySet.put("price", fields.get(7));
        } catch (CommandException err) {
            System.out.println(err.getMessage());
            return;
        }
        while (!queryIsFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to update");
            System.out.println(String.format("# 1 # Smartphone OS{%s}: ", (querySet.get("smartphoneOS"))));
            System.out.println(String.format("# 2 # Battery volume{%s}: ", (querySet.get("batteryVolume"))));
            System.out.println(String.format("# 3 # Version OS{%s}: ", (querySet.get("versionOS"))));
            System.out.println(String.format("# 4 # Full name{%s}: ", (querySet.get("fullName"))));
            System.out.println(String.format("# 5 # Manufacturer{%s}: ", (querySet.get("manufacturer"))));
            System.out.println(String.format("# 6 # Type of headphones connector{%s}: ", (querySet.get("typeOfHeadphonesConnector"))));
            System.out.println(String.format("# 7 # Type of charger connector{%s}: ", (querySet.get("typeOfChargerConnector"))));
            System.out.println(String.format("# 8 # Price{%s}: ", (querySet.get("price"))));
            System.out.println("# 9 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print("# Smartphone OS: ");
                    query = scn.nextLine();
                    querySet.put("smartphoneOS", query);
                    break;
                case ("2"):
                    System.out.print("# Battery volume: ");
                    query = scn.nextLine();
                    querySet.put("batteryVolume", query);
                    break;
                case ("3"):
                    System.out.print("# Version OS{%s}: ");
                    query = scn.nextLine();
                    querySet.put("versionOS", query);
                    break;
                case ("4"):
                    System.out.print("# Full name: ");
                    query = scn.nextLine();
                    querySet.put("fullName", query);
                    break;
                case ("5"):
                    System.out.print("# Manufacturer: ");
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("6"):
                    System.out.print("# Type of headphones connector{%s}: ");
                    query = scn.nextLine();
                    querySet.put("typeOfHeadphonesConnector", query);
                    break;
                case ("7"):
                    System.out.print("# Type of charger connector: ");
                    query = scn.nextLine();
                    querySet.put("typeOfChargerConnector", query);
                    break;
                case ("8"):
                    System.out.print("# Price: ");
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("9"):
                    queryIsFull = true;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        readyQuery.append(String.format(" '%s'", objId));
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(String.format(" '%s' '%s'", entry.getKey(), entry.getValue()));
        }
        try {
            controller.execute("smartphone 'update'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
        }
    }

    /**
     * Main entry procedure interactive visual work with objects. Starts with a show list all objects.
     */
    public static void showListSmartphones() {
        int currentPage = 0;
        String[] objects;
        ArrayList<Integer> idObjects = new ArrayList<>();
        try {
            String response = controller.execute(
                    String.format("smartphone 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
            objects = response.split("\n");
            for (var elem : objects) {
                if (!elem.isEmpty()) {
                    String[] line = elem.split(" ");
                    idObjects.add(Integer.parseInt(line[line.length - 1]));
                }
            }
        } catch (CommandException e) {
            System.out.println(e.getMessage());
            return;
        }

        while (true) {
            System.out.println("######################################");
            System.out.println("#            Smartphones             #");
            System.out.println("######################################");
            if (idObjects.size() != 0) {
                for (int i = 0; i < idObjects.size(); ++i) {
                    System.out.println(String.format("# %d # %s", i + 1, objects[i]));
                }
            } else System.out.println("#              Empty list            #");
            System.out.println("######################################");
            System.out.println("# + # Add new object");
            System.out.println("# @ # Update objects");
            System.out.println("# / # Sort objects");
            System.out.println("# s # Search object");
            System.out.println("# < # Previous page");
            System.out.println("# > # Next page");
            System.out.println("# 0 # Exit");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            switch (action) {
                case ("1"):
                    if (objects.length > 0)
                        showSmartphone(idObjects.get(0));
                    break;
                case ("2"):
                    if (objects.length > 1)
                        showSmartphone(idObjects.get(1));
                    break;
                case ("3"):
                    if (objects.length > 2)
                        showSmartphone(idObjects.get(2));
                    break;
                case ("4"):
                    if (objects.length > 3)
                        showSmartphone(idObjects.get(3));
                    break;
                case ("5"):
                    if (objects.length > 4)
                        showSmartphone(idObjects.get(4));
                    break;
                case ("6"):
                    if (objects.length > 5)
                        showSmartphone(idObjects.get(5));
                    break;
                case ("7"):
                    if (objects.length > 6)
                        showSmartphone(idObjects.get(6));
                    break;
                case ("8"):
                    if (objects.length > 7)
                        showSmartphone(idObjects.get(7));
                    break;
                case ("9"):
                    if (objects.length > 8)
                        showSmartphone(idObjects.get(8));
                    break;
                case ("10"):
                    if (objects.length > 9)
                        showSmartphone(idObjects.get(9));
                    break;
                case ("+"):
                    addSmartphone();
                    break;
                case ("@"):
                    try {
                        String response = controller.execute(
                                String.format("smartphone 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    } catch (CommandException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    break;
                case ("/"):
                    if (objects.length > 0) {
                        naturalSortChargers(objects);
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("s"):
                    String response = searchSmartphone();
                    if (response != null && !response.isEmpty()) {
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("<"):
                    currentPage -= 1;
                    break;
                case (">"):
                    currentPage += 1;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }
    }
}
