package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PowerBankView extends BaseView {

    /**
     * Procedure of visual interactive adding object
     */
    private static void addPowerBank() {
        String request = "";
        System.out.println("######################################");
        System.out.println("#           Add PowerBank            #");
        System.out.println("######################################");
        System.out.print("# Volume Power: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Manufacturer ID: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Count Connections: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Price: ");
        request += (" '" + scn.nextLine() + "'");

        try {
            controller.execute("powerbank 'add'" + request);
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive deleting object
     */
    private static void deletePowerBank(int objId) {
        try {
            controller.execute(String.format("powerbank 'delete' '%d'", objId));
        } catch (CommandException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive showing object
     */
    private static void showPowerBank(int objId) {
        try {
            String info = controller.execute(String.format("powerbank 'get' '%d'", objId));
            ArrayList<String> infoParams = decoder(info);
            while (true) {
                System.out.println("######################################");
                System.out.println(String.format("# Powerbank ID: %s", infoParams.get(4)));
                System.out.println(String.format("# Volume power: %s", infoParams.get(0)));
                System.out.println(String.format("# Manufacturer: %s", infoParams.get(1)));
                System.out.println(String.format("# Count connections: %s", infoParams.get(2)));
                System.out.println(String.format("# Price: %s", infoParams.get(3)));
                System.out.println("######################################");
                System.out.println("# 1 # Update");
                System.out.println("# 2 # Delete");
                System.out.println("# 0 # Exit");
                System.out.print("# Input action: ");
                String action = scn.nextLine();
                switch (action) {
                    case ("1"):
                        updatePowerBank(objId);
                        info = controller.execute(String.format("powerbank 'get' '%d'", objId));
                        infoParams = decoder(info);
                        break;
                    case ("2"):
                        deletePowerBank(objId);
                        return;
                    case ("0"):
                        return;
                    default:
                        break;
                }
            }
        } catch (CommandException err) {
            System.out.println("\n" + err.getMessage());
        }
    }

    /**
     * Procedure of visual interactive searching object
     *
     * @return string version of found objects
     */
    private static String searchPowerBank() {
        boolean queryIfFull = false;
        HashMap<String, String> querySet = new HashMap<>();

        while (!queryIfFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to search");
            System.out.println("# 1 # Volume power");
            System.out.println("# 2 # Manufacturer");
            System.out.println("# 3 # Count connections");
            System.out.println("# 4 # Price");
            System.out.println("# 5 # ID");
            System.out.println("# 6 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print(String.format("# Volume power{%s}: ",
                            (querySet.getOrDefault("volumePower", ""))));
                    query = scn.nextLine();
                    querySet.put("volumePower", query);
                    break;
                case ("2"):
                    System.out.print(String.format("# Manufacturer{%s}: ",
                            (querySet.getOrDefault("manufacturer", ""))));
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("3"):
                    System.out.print(String.format("# Count connections{%s}: ",
                            (querySet.getOrDefault("countConnections", ""))));
                    query = scn.nextLine();
                    querySet.put("countConnections", query);
                    break;
                case ("4"):
                    System.out.print(String.format("# Price{%s}: ",
                            (querySet.getOrDefault("price", ""))));
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("5"):
                    System.out.print(String.format("# ID{%s}: ",
                            (querySet.getOrDefault("ID", ""))));
                    query = scn.nextLine();
                    querySet.put("ID", query);
                    break;
                case ("6"):
                    queryIfFull = true;
                    break;
                case ("0"):
                    return null;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(" '").append(entry.getKey()).append("'");
            readyQuery.append(" '").append(entry.getValue()).append("'");
        }
        try {
            return controller.execute("powerbank 'search'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
            return null;
        }
    }

    /**
     * Procedure of visual interactive updating object
     */
    private static void updatePowerBank(int objId) {
        boolean queryIsFull = false;
        HashMap<String, String> querySet = new HashMap<>();
        ArrayList<String> fields;
        try {
            fields = decoder(controller.execute(String.format("powerbank 'get' '%d'", objId)));
            querySet.put("volumePower", fields.get(0));
            querySet.put("manufacturer", fields.get(1));
            querySet.put("countConnections", fields.get(2));
            querySet.put("price", fields.get(3));
        } catch (CommandException err) {
            System.out.println(err.getMessage());
            return;
        }
        while (!queryIsFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to update");
            System.out.println(String.format("# 1 # Volume power{%s}: ", (querySet.get("volumePower"))));
            System.out.println(String.format("# 2 # Manufacturer{%s}: ", (querySet.get("manufacturer"))));
            System.out.println(String.format("# 3 # Count connections{%s}: ", (querySet.get("countConnections"))));
            System.out.println(String.format("# 4 # Price{%s}: ", (querySet.get("price"))));
            System.out.println("# 5 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print("# Volume power: ");
                    query = scn.nextLine();
                    querySet.put("volumePower", query);
                    break;
                case ("2"):
                    System.out.print("# Manufacturer: ");
                    query = scn.nextLine();
                    querySet.put("manufacturer", query);
                    break;
                case ("3"):
                    System.out.print("# Count connections{%s}: ");
                    query = scn.nextLine();
                    querySet.put("countConnections", query);
                    break;
                case ("4"):
                    System.out.print("# Price: ");
                    query = scn.nextLine();
                    querySet.put("price", query);
                    break;
                case ("5"):
                    queryIsFull = true;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        readyQuery.append(String.format(" '%s'", objId));
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(String.format(" '%s' '%s'", entry.getKey(), entry.getValue()));
        }
        try {
            controller.execute("powerbank 'update'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
        }
    }

    /**
     * Main entry procedure interactive visual work with objects. Starts with a show list all objects.
     */
    public static void showListPowerBanks() {
        int currentPage = 0;
        String[] objects;
        ArrayList<Integer> idObjects = new ArrayList<>();
        try {
            String response = controller.execute(
                    String.format("powerbank 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
            objects = response.split("\n");
            for (var elem : objects) {
                if (!elem.isEmpty()) {
                    String[] line = elem.split(" ");
                    idObjects.add(Integer.parseInt(line[line.length - 1]));
                }
            }
        } catch (CommandException e) {
            System.out.println(e.getMessage());
            return;
        }

        while (true) {
            System.out.println("######################################");
            System.out.println("#             PowerBanks             #");
            System.out.println("######################################");
            if (idObjects.size() != 0) {
                for (int i = 0; i < idObjects.size(); ++i) {
                    System.out.println(String.format("# %d # %s", i + 1, objects[i]));
                }
            } else System.out.println("#              Empty list            #");
            System.out.println("######################################");
            System.out.println("# + # Add new object");
            System.out.println("# @ # Update objects");
            System.out.println("# / # Sort objects");
            System.out.println("# s # Search object");
            System.out.println("# < # Previous page");
            System.out.println("# > # Next page");
            System.out.println("# 0 # Exit");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            switch (action) {
                case ("1"):
                    if (objects.length > 0)
                        showPowerBank(idObjects.get(0));
                    break;
                case ("2"):
                    if (objects.length > 1)
                        showPowerBank(idObjects.get(1));
                    break;
                case ("3"):
                    if (objects.length > 2)
                        showPowerBank(idObjects.get(2));
                    break;
                case ("4"):
                    if (objects.length > 3)
                        showPowerBank(idObjects.get(3));
                    break;
                case ("5"):
                    if (objects.length > 4)
                        showPowerBank(idObjects.get(4));
                    break;
                case ("6"):
                    if (objects.length > 5)
                        showPowerBank(idObjects.get(5));
                    break;
                case ("7"):
                    if (objects.length > 6)
                        showPowerBank(idObjects.get(6));
                    break;
                case ("8"):
                    if (objects.length > 7)
                        showPowerBank(idObjects.get(7));
                    break;
                case ("9"):
                    if (objects.length > 8)
                        showPowerBank(idObjects.get(8));
                    break;
                case ("10"):
                    if (objects.length > 9)
                        showPowerBank(idObjects.get(9));
                    break;
                case ("+"):
                    addPowerBank();
                    break;
                case ("@"):
                    try {
                        String response = controller.execute(
                                String.format("powerbank 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    } catch (CommandException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    break;
                case ("/"):
                    if (objects.length > 0) {
                        naturalSortChargers(objects);
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("s"):
                    String response = searchPowerBank();
                    if (response != null && !response.isEmpty()) {
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("<"):
                    currentPage -= 1;
                    break;
                case (">"):
                    currentPage += 1;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }
    }
}
