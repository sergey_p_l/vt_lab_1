package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ManufacturerView extends BaseView {

    /**
     * Procedure of visual interactive adding object
     */
    private static void addManufacturer() {
        String request = "";
        System.out.println("######################################");
        System.out.println("#          Add Manufacturer          #");
        System.out.println("######################################");
        System.out.print("# Country: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# FullName: ");
        request += (" '" + scn.nextLine() + "'");

        try {
            controller.execute("manufacturer 'add'" + request);
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive deleting object
     */
    private static void deleteManufacturer(int objId) {
        try {
            controller.execute(String.format("manufacturer 'delete' '%d'", objId));
        } catch (CommandException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Procedure of visual interactive showing object
     */
    private static void showManufacturer(int objId) {
        try {
            String info = controller.execute(String.format("manufacturer 'get' '%d'", objId));
            ArrayList<String> infoParams = decoder(info);
            while (true) {
                System.out.println("######################################");
                System.out.println(String.format("# Manufacturer ID: %s", infoParams.get(2)));
                System.out.println(String.format("# Country: %s", infoParams.get(0)));
                System.out.println(String.format("# Full name: %s", infoParams.get(1)));
                System.out.println("######################################");
                System.out.println("# 1 # Update");
                System.out.println("# 2 # Delete");
                System.out.println("# 0 # Exit");
                System.out.print("# Input action: ");
                String action = scn.nextLine();
                switch (action) {
                    case ("1"):
                        updateManufacturer(objId);
                        info = controller.execute(String.format("manufacturer 'get' '%d'", objId));
                        infoParams = decoder(info);
                        break;
                    case ("2"):
                        deleteManufacturer(objId);
                        return;
                    case ("0"):
                        return;
                    default:
                        break;
                }
            }
        } catch (CommandException err) {
            System.out.println("\n" + err.getMessage());
        }
    }

    /**
     * Procedure of visual interactive searching object
     *
     * @return string version of found objects
     */
    private static String searchManufacturer() {
        boolean queryIfFull = false;
        HashMap<String, String> querySet = new HashMap<>();

        while (!queryIfFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to search");
            System.out.println("# 1 # Country");
            System.out.println("# 2 # Full name");
            System.out.println("# 3 # ID");
            System.out.println("# 4 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print(String.format("# Country{%s}: ",
                            (querySet.getOrDefault("country", ""))));
                    query = scn.nextLine();
                    querySet.put("country", query);
                    break;
                case ("2"):
                    System.out.print(String.format("# Full name{%s}: ",
                            (querySet.getOrDefault("fullName", ""))));
                    query = scn.nextLine();
                    querySet.put("fullName", query);
                    break;
                case ("3"):
                    System.out.print(String.format("# ID{%s}: ",
                            (querySet.getOrDefault("ID", ""))));
                    query = scn.nextLine();
                    querySet.put("ID", query);
                    break;
                case ("4"):
                    queryIfFull = true;
                    break;
                case ("0"):
                    return null;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(" '").append(entry.getKey()).append("'");
            readyQuery.append(" '").append(entry.getValue()).append("'");
        }
        try {
            return controller.execute("manufacturer 'search'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
            return null;
        }
    }

    /**
     * Procedure of visual interactive updating object
     */
    private static void updateManufacturer(int objId) {
        boolean queryIsFull = false;
        HashMap<String, String> querySet = new HashMap<>();
        ArrayList<String> fields;
        try {
            fields = decoder(controller.execute(String.format("manufacturer 'get' '%d'", objId)));
            querySet.put("country", fields.get(0));
            querySet.put("fullName", fields.get(1));
        } catch (CommandException err) {
            System.out.println(err.getMessage());
            return;
        }
        while (!queryIsFull) {
            System.out.println("######################################");
            System.out.println("# Choice the parameter to update");
            System.out.println(String.format("# 1 # Country{%s}: ",
                    (querySet.get("country"))));
            System.out.println(String.format("# 2 # Full name{%s}: ",
                    (querySet.get("fullName"))));
            System.out.println("# 3 # Search");
            System.out.println("# 0 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            String query;
            switch (action) {
                case ("1"):
                    System.out.print("# Country: ");
                    query = scn.nextLine();
                    querySet.put("country", query);
                    break;
                case ("2"):
                    System.out.print("# Full name: ");
                    query = scn.nextLine();
                    querySet.put("fullName", query);
                    break;
                case ("3"):
                    queryIsFull = true;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }

        StringBuilder readyQuery = new StringBuilder();
        readyQuery.append(String.format(" '%s'", objId));
        for (Map.Entry<String, String> entry : querySet.entrySet()) {
            readyQuery.append(String.format(" '%s' '%s'", entry.getKey(), entry.getValue()));
        }
        try {
            controller.execute("manufacturer 'update'" + readyQuery);
        } catch (CommandException e) {
            System.out.println("\n" + e.getMessage());
        }
    }

    /**
     * Main entry procedure interactive visual work with objects. Starts with a show list all objects.
     */
    public static void showListManufacturers() {
        int currentPage = 0;
        String[] objects;
        ArrayList<Integer> idObjects = new ArrayList<>();
        try {
            String response = controller.execute(
                    String.format("manufacturer 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
            objects = response.split("\n");
            for (var elem : objects) {
                if (!elem.isEmpty()) {
                    String[] line = elem.split(" ");
                    idObjects.add(Integer.parseInt(line[line.length - 1]));
                }
            }
        } catch (CommandException e) {
            System.out.println(e.getMessage());
            return;
        }

        while (true) {
            System.out.println("######################################");
            System.out.println("#            Manufacturers           #");
            System.out.println("######################################");
            if (idObjects.size() != 0) {
                for (int i = 0; i < idObjects.size(); ++i) {
                    System.out.println(String.format("# %d # %s", i + 1, objects[i]));
                }
            } else System.out.println("#              Empty list            #");
            System.out.println("######################################");
            System.out.println("# + # Add new object");
            System.out.println("# @ # Update objects");
            System.out.println("# / # Sort objects");
            System.out.println("# s # Search object");
            System.out.println("# < # Previous page");
            System.out.println("# > # Next page");
            System.out.println("# 0 # Exit");
            System.out.print("# Input action: ");
            String action = scn.nextLine();
            switch (action) {
                case ("1"):
                    if (objects.length > 0)
                        showManufacturer(idObjects.get(0));
                    break;
                case ("2"):
                    if (objects.length > 1)
                        showManufacturer(idObjects.get(1));
                    break;
                case ("3"):
                    if (objects.length > 2)
                        showManufacturer(idObjects.get(2));
                    break;
                case ("4"):
                    if (objects.length > 3)
                        showManufacturer(idObjects.get(3));
                    break;
                case ("5"):
                    if (objects.length > 4)
                        showManufacturer(idObjects.get(4));
                    break;
                case ("6"):
                    if (objects.length > 5)
                        showManufacturer(idObjects.get(5));
                    break;
                case ("7"):
                    if (objects.length > 6)
                        showManufacturer(idObjects.get(6));
                    break;
                case ("8"):
                    if (objects.length > 7)
                        showManufacturer(idObjects.get(7));
                    break;
                case ("9"):
                    if (objects.length > 8)
                        showManufacturer(idObjects.get(8));
                    break;
                case ("10"):
                    if (objects.length > 9)
                        showManufacturer(idObjects.get(9));
                    break;
                case ("+"):
                    addManufacturer();
                    break;
                case ("@"):
                    try {
                        String response = controller.execute(
                                String.format("manufacturer 'show' '%d' '%d'", currentPage * 10, (currentPage + 1) * 10));
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    } catch (CommandException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    break;
                case ("/"):
                    if (objects.length > 0) {
                        naturalSortChargers(objects);
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("s"):
                    String response = searchManufacturer();
                    if (response != null && !response.isEmpty()) {
                        objects = response.split("\n");
                        idObjects.clear();
                        for (var elem : objects) {
                            if (!elem.isEmpty()) {
                                String[] line = elem.split(" ");
                                idObjects.add(Integer.parseInt(line[line.length - 1]));
                            }
                        }
                    }
                    break;
                case ("<"):
                    currentPage -= 1;
                    break;
                case (">"):
                    currentPage += 1;
                    break;
                case ("0"):
                    return;
                default:
                    break;
            }
        }
    }
}
