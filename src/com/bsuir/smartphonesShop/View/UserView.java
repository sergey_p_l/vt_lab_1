package com.bsuir.smartphonesShop.View;

import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.Entity.User;

import java.util.ArrayList;

public class UserView extends BaseView {

    /**
     * Procedure of visual interactive entering in system.
     *
     * @return string information about entered user in system.
     */
    public static User enterAccount() {
        User user = null;
        String action;
        do {
            System.out.println("######################################");
            System.out.println("# Welcome to Smartphones Online Shop #");
            System.out.println("######################################");
            System.out.println("# 1 # Registration");
            System.out.println("# 2 # Authorization");
            System.out.println("# 3 # Exit");
            System.out.println("######################################");
            System.out.print("# Input action: ");
            action = scn.nextLine();
            switch (action) {
                case ("1"): {
                    user = addUser();
                    break;
                }

                case ("2"): {
                    user = authUser();
                    break;
                }
                case ("3"): {
                    return null;
                }
                default:
                    break;
            }
        } while (user == null);
        return user;
    }

    /**
     * Procedure of visual interactive registration new user.
     *
     * @return string information about new user.
     */
    private static User addUser() {
        String request = "";
        System.out.println("######################################");
        System.out.println("#            REGISTRATION            #");
        System.out.println("######################################");
        System.out.print("# Input first name: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Input second name: ");
        request += (" '" + scn.nextLine() + "'");
        System.out.print("# Input login: ");
        request += (" '" + scn.nextLine() + "'");
        String password1 = "", password2 = "";
        do {
            if (!password1.equals(password2))
                System.out.println("# ERROR. Passwords do not match. Try again.");
            System.out.print("# Input password: ");
            password1 = scn.nextLine();
            System.out.print("# Confirm password: ");
            password2 = scn.nextLine();
        } while (!password1.equals(password2));
        request += (" '" + password1 + "'");
        System.out.print("# Input email: ");
        request += (" '" + scn.nextLine() + "'");

        try {
            String response = controller.execute("user 'add'" + request);
            ArrayList<String> data = decoder(response);
            if (data.size() > 1) {
                User user = new User();
                user.setName(data.get(0));
                user.setSurname(data.get(1));
                user.setLogin(data.get(2));
                user.setPassword(data.get(3));
                user.setEmail(data.get(4));
                user.setIsAdmin(data.get(5).equals("True"));
                user.setID(Integer.parseInt(data.get(6)));
                return user;
            } else
                System.out.println(data.get(0));
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());

        }
        return null;
    }

    /**
     * Procedure of visual interactive user authenticate.
     *
     * @return string information about authenticated user.
     */
    private static User authUser() {
        String request = "user 'auth'";
        System.out.println("######################################");
        System.out.println("#           AUTHORIZATION            #");
        System.out.println("######################################");
        System.out.print("# Input Login: ");
        request += String.format(" 'login' '%s'", scn.nextLine());
        System.out.print("# Input password: ");
        request += String.format(" 'password' '%s'", scn.nextLine());

        try {
            String response = controller.execute(request);
            ArrayList<String> data = decoder(response);
            User user = new User();
            user.setName(data.get(0));
            user.setSurname(data.get(1));
            user.setLogin(data.get(2));
            user.setPassword(data.get(3));
            user.setEmail(data.get(4));
            user.setIsAdmin(data.get(5).equals("True"));
            user.setID(Integer.parseInt(data.get(6)));
            return user;
        } catch (CommandException e) {
            System.out.println("\nError: " + e.getMessage());
        }
        return null;
    }
}
