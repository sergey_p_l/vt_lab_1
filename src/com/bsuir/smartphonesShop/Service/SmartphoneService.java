package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.SmartphoneDAO;
import com.bsuir.smartphonesShop.DAL.DAO.DAOFactory;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Smartphone;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class SmartphoneService extends BaseService<SmartphoneDAO, Smartphone> {

    /**
     * @return DAO instance for working with current model.
     */
    @Override
    protected SmartphoneDAO getDAO() {
        DAOFactory factory = DAOFactory.getInstance();
        return factory.getSmartphoneDAO();
    }

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean addObject(ArrayList<String> fields) throws ServiceException {
        try {
            SmartphoneDAO smartphoneDAO = getDAO();
            smartphoneDAO.addItem(fields);
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    @Override
    public LinkedList<Smartphone> searchObjects(HashMap<String, String> query) {
        SmartphoneDAO smartphoneDAO = getDAO();
        return smartphoneDAO.searchItem(query);
    }

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean updateObject(String id, ArrayList<String> fields) throws ServiceException {
        try {
            SmartphoneDAO smartphoneDAO = getDAO();
            return smartphoneDAO.updateItem(Integer.parseInt(id), fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
