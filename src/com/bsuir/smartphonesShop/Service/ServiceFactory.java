package com.bsuir.smartphonesShop.Service;

public class ServiceFactory {
    private static final ServiceFactory factory = new ServiceFactory();

    private final ChargerService chargerService = new ChargerService();
    private final HeadphonesService headphonesService = new HeadphonesService();
    private final ManufacturerService manufacturerService = new ManufacturerService();
    private final PowerBankService powerBankService = new PowerBankService();
    private final SmartphoneService smartphoneService = new SmartphoneService();
    private final UserService userService = new UserService();

    /**
     * @return self instance.
     */
    public static ServiceFactory getInstance() {
        return factory;
    }

    /**
     * @return service instance for working with Chargers.
     */
    public ChargerService getChargerService() {
        return chargerService;
    }

    /**
     * @return service instance for working with Headphones.
     */
    public HeadphonesService getHeadphonesService() {
        return headphonesService;
    }

    /**
     * @return service instance for working with Manufacturers.
     */
    public ManufacturerService getManufacturerService() {
        return manufacturerService;
    }

    /**
     * @return service instance for working with PowerBanks.
     */
    public PowerBankService getPowerBankService() {
        return powerBankService;
    }

    /**
     * @return service instance for working with Smartphones.
     */
    public SmartphoneService getSmartphoneService() {
        return smartphoneService;
    }

    /**
     * @return service instance for working with Users.
     */
    public UserService getUserService() {
        return userService;
    }
}
