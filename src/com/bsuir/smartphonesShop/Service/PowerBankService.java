package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.DAOFactory;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.DAO.PowerBankDAO;
import com.bsuir.smartphonesShop.DAL.Entity.PowerBank;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class PowerBankService extends BaseService<PowerBankDAO, PowerBank> {

    /**
     * @return DAO instance for working with current model.
     */
    @Override
    protected PowerBankDAO getDAO() {
        DAOFactory factory = DAOFactory.getInstance();
        return factory.getPowerBankDAO();
    }

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean addObject(ArrayList<String> fields) throws ServiceException {
        try {
            PowerBankDAO powerBankDAO = getDAO();
            return powerBankDAO.addItem(fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    @Override
    public LinkedList<PowerBank> searchObjects(HashMap<String, String> query) {
        PowerBankDAO powerBankDAO = getDAO();
        return powerBankDAO.searchItem(query);
    }

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean updateObject(String id, ArrayList<String> fields) throws ServiceException {
        try {
            PowerBankDAO powerBankDAO = getDAO();
            return powerBankDAO.updateItem(Integer.parseInt(id), fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
