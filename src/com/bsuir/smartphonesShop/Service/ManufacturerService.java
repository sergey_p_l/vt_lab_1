package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.DAOFactory;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.DAO.ManufacturerDAO;
import com.bsuir.smartphonesShop.DAL.Entity.Manufacturer;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class ManufacturerService extends BaseService<ManufacturerDAO, Manufacturer> {

    /**
     * @return DAO instance for working with current model.
     */
    @Override
    protected ManufacturerDAO getDAO() {
        DAOFactory factory = DAOFactory.getInstance();
        return factory.getManufacturerDAO();
    }

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean addObject(ArrayList<String> fields) throws ServiceException {
        try {
            ManufacturerDAO manufacturerDAO = getDAO();
            return manufacturerDAO.addItem(fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    @Override
    public LinkedList<Manufacturer> searchObjects(HashMap<String, String> query) {
        ManufacturerDAO manufacturerDAO = getDAO();
        return manufacturerDAO.searchItem(query);
    }

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean updateObject(String id, ArrayList<String> fields) throws ServiceException {
        try {
            ManufacturerDAO manufacturerDAO = getDAO();
            return manufacturerDAO.updateItem(Integer.parseInt(id), fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
