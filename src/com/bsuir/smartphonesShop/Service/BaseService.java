package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.BaseDAO;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.IdClass;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public abstract class BaseService<T extends BaseDAO, K extends IdClass> {

    /**
     * @return DAO instance for working with current model.
     */
    protected abstract T getDAO();

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    public abstract boolean addObject(ArrayList<String> fields) throws ServiceException;

    /**
     * @param idObj is id of deleting object.
     * @return true, if object successfully deleted. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    public boolean deleteObject(int idObj) throws ServiceException {
        try {
            T dao = getDAO();
            return dao.deleteItem(idObj);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param idObj is id of deleting object.
     * @return object with given id.
     */
    public K getObject(int idObj) {
        T dao = getDAO();
        return (K) dao.getItem(idObj);
    }

    /**
     * @param start start index of total list objects.
     * @param end   end index of total list objects.
     * @return list with object with id in given range.
     */
    public LinkedList<K> showObjects(String start, String end) {
        T dao = getDAO();
        return new LinkedList<K>(dao.getAll(Integer.parseInt(start), Integer.parseInt(end)));
    }

    /**
     * @return true, if all objects from db file successfully loaded. Else throw ServiceException.
     * @throws DAOException throws if received DAOException.
     */
    public boolean loadObjects() throws DAOException {
        T dao = getDAO();
        dao.readAll();
        return true;
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    public abstract LinkedList<K> searchObjects(HashMap<String, String> query);

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    public abstract boolean updateObject(String id, ArrayList<String> fields) throws ServiceException;
}
