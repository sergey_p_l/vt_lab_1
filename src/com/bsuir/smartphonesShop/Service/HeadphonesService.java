package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.HeadphonesDAO;
import com.bsuir.smartphonesShop.DAL.DAO.DAOFactory;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.Entity.Headphones;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class HeadphonesService extends BaseService<HeadphonesDAO, Headphones> {

    /**
     * @return DAO instance for working with current model.
     */
    @Override
    protected HeadphonesDAO getDAO() {
        DAOFactory factory = DAOFactory.getInstance();
        return factory.getHeadphonesDAO();
    }

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean addObject(ArrayList<String> fields) throws ServiceException {
        try {
            HeadphonesDAO headphonesDAO = getDAO();
            headphonesDAO.addItem(fields);
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    @Override
    public LinkedList<Headphones> searchObjects(HashMap<String, String> query) {
        HeadphonesDAO headphonesDAO = getDAO();
        return headphonesDAO.searchItem(query);
    }

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean updateObject(String id, ArrayList<String> fields) throws ServiceException {
        try {
            HeadphonesDAO headphonesDAO = getDAO();
            return headphonesDAO.updateItem(Integer.parseInt(id), fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}