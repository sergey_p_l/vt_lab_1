package com.bsuir.smartphonesShop.Service;

import com.bsuir.smartphonesShop.DAL.DAO.DAOFactory;
import com.bsuir.smartphonesShop.DAL.DAO.Exception.DAOException;
import com.bsuir.smartphonesShop.DAL.DAO.UserDAO;
import com.bsuir.smartphonesShop.DAL.Entity.User;
import com.bsuir.smartphonesShop.Service.Exception.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class UserService extends BaseService<UserDAO, User> {

    /**
     * @return DAO instance for working with current model.
     */
    @Override
    protected UserDAO getDAO() {
        DAOFactory factory = DAOFactory.getInstance();
        return factory.getUserDAO();
    }

    /**
     * @param fields is list with string values of fields of new object.
     * @return true, if object successfully created and save. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean addObject(ArrayList<String> fields) throws ServiceException {
        try {
            UserDAO userDAO = getDAO();
            return userDAO.addItem(fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param query is dictionary with names of fields and string values for searching objects.
     * @return list with searching objects.
     */
    @Override
    public LinkedList<User> searchObjects(HashMap<String, String> query) {
        UserDAO userDAO = getDAO();
        return userDAO.searchItem(query);
    }

    /**
     * @param fields is dictionary with auth information.
     * @return list with authenticated users.
     */
    public ArrayList<User> auth(HashMap<String, String> fields) {
        UserDAO userDAO = getDAO();
        return userDAO.authUser(fields.get("login"), fields.get("password"));
    }

    /**
     * @param id     is id updating object.
     * @param fields is list with string values of fields of updating object.
     * @return true, if object successfully updated. Else throw ServiceException.
     * @throws ServiceException throws if received DAOException.
     */
    @Override
    public boolean updateObject(String id, ArrayList<String> fields) throws ServiceException {
        try {
            UserDAO userDAO = getDAO();
            return userDAO.updateItem(Integer.parseInt(id), fields);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
