package com.bsuir.smartphonesShop.Repository;

public interface DAO<T> {

	void save(Iterable<T> objects);
	Iterable<T> read();
}
