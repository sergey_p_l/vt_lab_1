package com.bsuir.smartphonesShop.Repository;

import com.bsuir.smartphonesShop.DAL.Entity.*;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.XMLDAO;
import com.bsuir.smartphonesShop.Repository.MySqlRepository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Transfer {

	private static final Logger logger = LogManager.getLogger();

	public static void main(String[] args) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File("db.properties")));
		} catch (IOException exc) {
			System.out.println("Can't load properties");
			return;
		}

		String xmlPath = properties.get("xmlPath").toString();
		String xsdPath = properties.get("xsdPath").toString();

		XMLDAO<User> userXMLDAO = new XMLDAO<>(
				new File(xmlPath + "users.xml"), new File(xsdPath + "users.xsd"));
		XMLDAO<Manufacturer> manufacturerXMLDAO = new XMLDAO<>(
				new File(xmlPath + "manufacturers.xml"), new File(xsdPath + "manufacturers.xsd"));
		XMLDAO<Smartphone> smartphoneXMLDAO = new XMLDAO<>(
				new File(xmlPath + "smartphones.xml"), new File(xsdPath + "smartphones.xsd"));
		XMLDAO<Charger> chargerXMLDAO = new XMLDAO<>(
				new File(xmlPath + "chargers.xml"), new File(xsdPath + "chargers.xsd"));
		XMLDAO<Headphones> headphonesXMLDAO = new XMLDAO<>(
				new File(xmlPath + "headphones.xml"), new File(xsdPath + "headphones.xsd"));
		XMLDAO<PowerBank> powerBankXMLDAO = new XMLDAO<>(
				new File(xmlPath + "powerbanks.xml"), new File(xsdPath + "powerbanks.xsd"));

		List<User> users = StreamSupport
				.stream(userXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read users: " + users.size());

		List<Manufacturer> manufacturers = StreamSupport
				.stream(manufacturerXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read manufacturers: " + manufacturers.size());

		List<Smartphone> smartphones = StreamSupport
				.stream(smartphoneXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read smartphones: " + smartphones.size());

		List<Charger> chargers = StreamSupport
				.stream(chargerXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read chargers: " + chargers.size());

		List<Headphones> headphones = StreamSupport
				.stream(headphonesXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read headphones: " + headphones.size());

		List<PowerBank> powerBanks = StreamSupport
				.stream(powerBankXMLDAO.read().spliterator(), false)
				.collect(Collectors.toList());
		logger.info("Read powerbanks: " + powerBanks.size());

		MySqlConfigurationManager configurationManager = MySqlConfigurationManager.getInstance(
				properties.get("host").toString(),
				properties.get("login").toString(),
				properties.get("password").toString()
		);
		MySqlUserDAO mySqlUserDAO = new MySqlUserDAO(configurationManager);
		MySqlManufacturersDAO mySqlManufacturersDAO = new MySqlManufacturersDAO(configurationManager);
		MySqlSmartphonesDAO mySqlSmartphonesDAO = new MySqlSmartphonesDAO(configurationManager, mySqlManufacturersDAO);
		MySqlChargersDAO mySqlChargersDAO = new MySqlChargersDAO(configurationManager, mySqlManufacturersDAO);
		MySqlHeadphonesDAO mySqlHeadphonesDAO = new MySqlHeadphonesDAO(configurationManager, mySqlManufacturersDAO);
		MySqlPowerBanksDAO mySqlPowerBanksDAO = new MySqlPowerBanksDAO(configurationManager, mySqlManufacturersDAO);

		mySqlUserDAO.clear();
		mySqlSmartphonesDAO.clear();
		mySqlChargersDAO.clear();
		mySqlHeadphonesDAO.clear();
		mySqlPowerBanksDAO.clear();
		mySqlManufacturersDAO.clear();

		mySqlUserDAO.save(users);
		mySqlManufacturersDAO.save(manufacturers);
		mySqlSmartphonesDAO.save(smartphones);
		mySqlChargersDAO.save(chargers);
		mySqlHeadphonesDAO.save(headphones);
		mySqlPowerBanksDAO.save(powerBanks);
	}
}
