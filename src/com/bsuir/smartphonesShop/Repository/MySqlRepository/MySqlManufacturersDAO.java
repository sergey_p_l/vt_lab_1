package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.Manufacturer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class MySqlManufacturersDAO extends MySqlDAO<Manufacturer> {

	public MySqlManufacturersDAO(MySqlConfigurationManager configurationManager) {
		super(configurationManager);
	}

	private Connection getConnection() throws SQLException {
		return MySqlConfigurationManager.getInstance().getConnection();
	}

	@Override
	String getTableName() {
		return "manufacturers";
	}

	@Override
	List<String> getFields() {
		return Arrays.asList("country", "fullName");
	}

	@Override
	void fillPreparedStatement(PreparedStatement preparedStatement, Manufacturer object) throws SQLException {
		preparedStatement.setString(1, object.getCountry());
		preparedStatement.setString(2, object.getFullName());
	}

	@Override
	Manufacturer getObject(ResultSet resultSet) throws SQLException {
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setCountry(resultSet.getString(1));
		manufacturer.setFullName(resultSet.getString(2));
		return manufacturer;
	}

	Manufacturer getByFullName(String fullName) throws SQLException {
		Manufacturer manufacturer = null;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM manufacturers WHERE fullName = ?;");
		preparedStatement.setString(1, fullName);
		preparedStatement.execute();
		ResultSet resultSet = preparedStatement.getResultSet();
		while (resultSet.next()) {
			manufacturer = new Manufacturer();
			manufacturer.setCountry(resultSet.getString(1));
			manufacturer.setFullName(resultSet.getString(2));
		}
		resultSet.close();
		preparedStatement.close();
		connection.close();
		return manufacturer;
	}
}
