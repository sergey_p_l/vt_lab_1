package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.Headphones;
import com.bsuir.smartphonesShop.Repository.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlHeadphonesDAO implements DAO<Headphones> {

	private MySqlManufacturersDAO manufacturersDAO;
	private MySqlConfigurationManager configurationManager;

	public MySqlHeadphonesDAO(MySqlConfigurationManager configurationManager, MySqlManufacturersDAO mySqlManufacturersDAO) {
		this.configurationManager = configurationManager;
		this.manufacturersDAO = mySqlManufacturersDAO;
	}

	private Connection getConnection() throws SQLException {
		return configurationManager.getConnection();
	}

	@Override
	public void save(Iterable<Headphones> objects) {
		objects.forEach(x -> {
			try {
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(
						"INSERT INTO `headphones` (type, typeOfConnector, manufacturer) VALUES (?, ?, ?);");
				preparedStatement.setString(1, x.getType());
				preparedStatement.setString(2, x.getTypeOfConnector());
				preparedStatement.setString(3, x.getManufacturer().getFullName());
				preparedStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public Iterable<Headphones> read() {
		List<Headphones> ans = new ArrayList<>();
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM headphones;");
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				Headphones headphones = new Headphones();
				headphones.setType(resultSet.getString(1));
				headphones.setTypeOfConnector(resultSet.getString(2));
				headphones.setManufacturer(manufacturersDAO.getByFullName(resultSet.getString(3)));
				ans.add(headphones);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public void clear() {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM headphones");
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
	}
}
