package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.PowerBank;
import com.bsuir.smartphonesShop.Repository.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlPowerBanksDAO implements DAO<PowerBank> {

	private MySqlManufacturersDAO manufacturersDAO;
	private MySqlConfigurationManager configurationManager;

	public MySqlPowerBanksDAO(MySqlConfigurationManager configurationManager, MySqlManufacturersDAO mySqlManufacturersDAO) {
		this.configurationManager = configurationManager;
		this.manufacturersDAO = mySqlManufacturersDAO;
	}

	private Connection getConnection() throws SQLException {
		return configurationManager.getConnection();
	}

	@Override
	public void save(Iterable<PowerBank> objects) {
		objects.forEach(x -> {
			try {
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(
						"INSERT INTO `powerbanks` (volumePower, manufacturer, countConnections) VALUES (?, ?, ?);");
				preparedStatement.setInt(1, x.getVolumePower());
				preparedStatement.setString(2, x.getManufacturer().getFullName());
				preparedStatement.setInt(3, x.getCountConnections());
				preparedStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public Iterable<PowerBank> read() {
		List<PowerBank> ans = new ArrayList<>();
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM powerbanks;");
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				PowerBank powerBank = new PowerBank();
				powerBank.setVolumePower(resultSet.getInt(1));
				powerBank.setManufacturer(manufacturersDAO.getByFullName(resultSet.getString(2)));
				powerBank.setCountConnections(resultSet.getInt(3));
				ans.add(powerBank);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public void clear() {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM powerbanks");
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
	}
}
