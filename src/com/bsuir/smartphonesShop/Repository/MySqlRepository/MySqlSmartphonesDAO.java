package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.Smartphone;
import com.bsuir.smartphonesShop.Repository.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlSmartphonesDAO implements DAO<Smartphone> {

	private MySqlConfigurationManager configurationManager;
	private MySqlManufacturersDAO manufacturersDAO;

	public MySqlSmartphonesDAO(MySqlConfigurationManager configurationManager, MySqlManufacturersDAO manufacturersDAO) {
		this.configurationManager = configurationManager;
		this.manufacturersDAO = manufacturersDAO;
	}

	private Connection getConnection() throws SQLException {
		return configurationManager.getConnection();
	}

	@Override
	public void save(Iterable<Smartphone> objects) {
		objects.forEach(x -> {
			try {
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(
						"INSERT INTO `smartphones` (smartphoneOS, batteryVolume, versionOS, fullName, manufacturer, typeOfHeadphonesConnector, typeOfChargerConnector) VALUES (?, ?, ?, ?, ?, ?, ?);");
				preparedStatement.setString(1, x.getSmartphoneOS());
				preparedStatement.setInt(2, x.getBatteryVolume());
				preparedStatement.setString(3, x.getVersionOS());
				preparedStatement.setString(4, x.getFullName());
				preparedStatement.setString(5, x.getManufacturer().getFullName());
				preparedStatement.setString(6, x.getTypeOfHeadphonesConnector());
				preparedStatement.setString(7, x.getTypeOfChargerConnector());
				preparedStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public Iterable<Smartphone> read() {
		List<Smartphone> ans = new ArrayList<>();
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM smartphones;");
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				Smartphone smartphone = new Smartphone();
				smartphone.setSmartphoneOS(resultSet.getString(1));
				smartphone.setBatteryVolume(resultSet.getInt(2));
				smartphone.setVersionOS(resultSet.getString(3));
				smartphone.setFullName(resultSet.getString(4));
				smartphone.setManufacturer(manufacturersDAO.getByFullName(resultSet.getString(5)));
				smartphone.setTypeOfHeadphonesConnector(resultSet.getString(6));
				smartphone.setTypeOfChargerConnector(resultSet.getString(7));
				ans.add(smartphone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public void clear() {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM smartphones");
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
	}
}
