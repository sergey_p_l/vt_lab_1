package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.Charger;
import com.bsuir.smartphonesShop.Repository.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlChargersDAO implements DAO<Charger> {

	private MySqlManufacturersDAO manufacturersDAO;
	private MySqlConfigurationManager configurationManager;

	public MySqlChargersDAO(MySqlConfigurationManager configurationManager, MySqlManufacturersDAO mySqlManufacturersDAO) {
		this.configurationManager = configurationManager;
		this.manufacturersDAO = mySqlManufacturersDAO;
	}

	private Connection getConnection() throws SQLException {
		return configurationManager.getConnection();
	}

	@Override
	public void save(Iterable<Charger> objects) {
		objects.forEach(x -> {
			try {
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(
						"INSERT INTO `chargers` (typeOfConnector, length, color, manufacturer) VALUES (?, ?, ?, ?);");
				preparedStatement.setString(1, x.getTypeOfConnector());
				preparedStatement.setDouble(2, x.getLength());
				preparedStatement.setString(3, x.getColor());
				preparedStatement.setString(4, x.getManufacturer().getFullName());
				preparedStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public Iterable<Charger> read() {
		List<Charger> ans = new ArrayList<>();
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM chargers;");
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				Charger charger = new Charger();
				charger.setTypeOfConnector(resultSet.getString(1));
				charger.setLength(resultSet.getDouble(2));
				charger.setColor(resultSet.getString(3));
				charger.setManufacturer(manufacturersDAO.getByFullName(resultSet.getString(4)));
				ans.add(charger);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public void clear() {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM chargers");
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
	}
}
