package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.Repository.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class MySqlDAO<T> implements DAO<T> {

	private MySqlConfigurationManager configurationManager;

	public MySqlDAO(MySqlConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	private Connection getConnection() throws SQLException {
		return configurationManager.getConnection();
	}

	protected String getInsertRequest() {
		return "INSERT INTO `" + getTableName() + "` (" + convertFieldsToRequestForm() + ") VALUES " + convertValuesToRequestForm() + ';';
	}

	protected String getSelectRequest() {
		return "SELECT " + convertFieldsToRequestForm() + " FROM " + getTableName() + ';';
	}

	abstract String getTableName();

	abstract List<String> getFields();

	private String convertFieldsToRequestForm() {
		return getFields().stream()
				.map(x -> '`' + x + '`')
				.collect(Collectors.joining(", "));
	}

	private String convertValuesToRequestForm() {
		return getFields().stream()
				.map(x -> "?")
				.collect(Collectors.joining(",", "(", ")"));
	}

	abstract void fillPreparedStatement(PreparedStatement preparedStatement, T object) throws SQLException;

	abstract T getObject(ResultSet resultSet) throws SQLException;


	public void save(Iterable<T> objects) {
		objects.forEach(x -> {
			try {
				Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(getInsertRequest());
				fillPreparedStatement(preparedStatement, x);
				preparedStatement.execute();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public Iterable<T> read() {
		List<T> list = new ArrayList<>();
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(getSelectRequest());
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				list.add(getObject(resultSet));
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void clear() {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM " + getTableName());
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
	}
}
