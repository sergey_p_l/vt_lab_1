package com.bsuir.smartphonesShop.Repository.MySqlRepository;

import com.bsuir.smartphonesShop.DAL.Entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class MySqlUserDAO extends MySqlDAO<User> {

	public MySqlUserDAO(MySqlConfigurationManager configurationManager) {
		super(configurationManager);
	}

	@Override
	String getTableName() {
		return "users";
	}

	@Override
	List<String> getFields() {
		return Arrays.asList("name", "surname", "login", "password", "email", "isAdmin");
	}

	@Override
	void fillPreparedStatement(PreparedStatement preparedStatement, User object) throws SQLException {
		preparedStatement.setString(1, object.getName());
		preparedStatement.setString(2, object.getSurname());
		preparedStatement.setString(3, object.getLogin());
		preparedStatement.setString(4, object.getPassword());
		preparedStatement.setString(5, object.getEmail());
		preparedStatement.setBoolean(6, object.getIsAdmin());
	}

	@Override
	User getObject(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setName(resultSet.getString(1));
		user.setSurname(resultSet.getString(2));
		user.setLogin(resultSet.getString(3));
		user.setPassword(resultSet.getString(4));
		user.setEmail(resultSet.getString(5));
		user.setIsAdmin(resultSet.getBoolean(6));
		return user;
	}
}
