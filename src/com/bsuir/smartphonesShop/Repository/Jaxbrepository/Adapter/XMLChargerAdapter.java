package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.Charger;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLCharger;

public class XMLChargerAdapter {

	public static XMLCharger toXml(Charger charger) {
		XMLCharger xmlCharger = new XMLCharger();
		xmlCharger.setPrice(charger.getPrice());
		xmlCharger.setColor(charger.getColor());
		xmlCharger.setLength(charger.getLength());
		xmlCharger.setTypeOfConnector(charger.getTypeOfConnector());
		xmlCharger.setManufacturer(XMLManufacturerAdapter.toXML(charger.getManufacturer()));
		return xmlCharger;
	}

	public static Charger toCharger(XMLCharger xmlCharger) {
		Charger charger = new Charger();
		charger.setPrice(xmlCharger.getPrice());
		charger.setColor(xmlCharger.getColor());
		charger.setLength(xmlCharger.getLength());
		charger.setTypeOfConnector(xmlCharger.getTypeOfConnector());
		charger.setManufacturer(XMLManufacturerAdapter.toManufacturer(xmlCharger.getManufacturer()));
		return charger;
	}
}
