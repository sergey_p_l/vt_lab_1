package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"typeOfConnector", "length", "color", "manufacturer"})
public class XMLCharger extends XMLProduct {

	private String typeOfConnector;
	private double length;
	private String color;
	private XMLManufacturer manufacturer;

	public XMLCharger() {
	}

	@XmlElement(name = "typeOfConnector", nillable = true)
	public String getTypeOfConnector() {
		return typeOfConnector;
	}

	public void setTypeOfConnector(String typeOfConnector) {
		this.typeOfConnector = typeOfConnector;
	}

	@XmlElement(name = "length")
	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	@XmlElement(name = "color", nillable = true)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@XmlElement(name = "manufacturer", nillable = true)
	public XMLManufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(XMLManufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}
}
