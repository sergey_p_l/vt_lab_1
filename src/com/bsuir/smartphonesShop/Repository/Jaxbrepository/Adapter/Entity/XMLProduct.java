package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"price"})
public class XMLProduct {

	private double price;

	public XMLProduct() {

	}

	@XmlElement(name = "price")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
