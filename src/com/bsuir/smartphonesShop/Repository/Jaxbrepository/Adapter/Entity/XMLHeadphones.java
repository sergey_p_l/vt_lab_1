package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"type", "typeOfConnector", "manufacturer"})
public class XMLHeadphones extends XMLProduct {

	private String type;
	private String typeOfConnector;
	private XMLManufacturer manufacturer;

	public XMLHeadphones() {
	}

	@XmlElement(name = "type", nillable = true)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "typeOfConnector", nillable = true)
	public String getTypeOfConnector() {
		return typeOfConnector;
	}

	public void setTypeOfConnector(String typeOfConnector) {
		this.typeOfConnector = typeOfConnector;
	}

	@XmlElement(name = "manufacturer", nillable = true)
	public XMLManufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(XMLManufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}
}
