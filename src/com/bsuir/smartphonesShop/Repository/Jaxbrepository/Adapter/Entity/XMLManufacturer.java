package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"country", "fullName"})
public class XMLManufacturer {

	private String country;
	private String fullName;

	public XMLManufacturer() {
	}

	@XmlElement(name = "country", nillable = true)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@XmlElement(name = "fullName", nillable = true)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
