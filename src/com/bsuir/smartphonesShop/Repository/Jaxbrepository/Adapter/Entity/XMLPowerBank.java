package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"volumePower", "manufacturer", "countConnections"})
public class XMLPowerBank extends XMLProduct {

	private int volumePower;
	private XMLManufacturer manufacturer;
	private int countConnections;

	public XMLPowerBank() {
	}

	@XmlElement(name = "volumePower")
	public int getVolumePower() {
		return volumePower;
	}

	public void setVolumePower(int volumePower) {
		this.volumePower = volumePower;
	}

	@XmlElement(name = "manufacturer", nillable = true)
	public XMLManufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(XMLManufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	@XmlElement(name = "countConnections")
	public int getCountConnections() {
		return countConnections;
	}

	public void setCountConnections(int countConnections) {
		this.countConnections = countConnections;
	}
}
