package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"smartphoneOS", "batteryVolume", "versionOS", "fullName", "manufacturer", "typeOfHeadphonesConnector", "typeOfChargerConnector"})
public class XMLSmartphone extends XMLProduct {

	private String smartphoneOS;
	private int batteryVolume;
	private String versionOS;
	private String fullName;
	private XMLManufacturer manufacturer;
	private String typeOfHeadphonesConnector;
	private String typeOfChargerConnector;

	public XMLSmartphone() {
	}

	@XmlElement(name = "smartphoneOS", nillable = true)
	public String getSmartphoneOS() {
		return smartphoneOS;
	}

	public void setSmartphoneOS(String smartphoneOS) {
		this.smartphoneOS = smartphoneOS;
	}

	@XmlElement(name = "batteryVolume")
	public int getBatteryVolume() {
		return batteryVolume;
	}

	public void setBatteryVolume(int batteryVolume) {
		this.batteryVolume = batteryVolume;
	}

	@XmlElement(name = "versionOS", nillable = true)
	public String getVersionOS() {
		return versionOS;
	}

	public void setVersionOS(String versionOS) {
		this.versionOS = versionOS;
	}

	@XmlElement(name = "fullName", nillable = true)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@XmlElement(name = "manufacturer", nillable = true)
	public XMLManufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(XMLManufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	@XmlElement(name = "typeOfHeadphonesConnector", nillable = true)
	public String getTypeOfHeadphonesConnector() {
		return typeOfHeadphonesConnector;
	}

	public void setTypeOfHeadphonesConnector(String typeOfHeadphonesConnector) {
		this.typeOfHeadphonesConnector = typeOfHeadphonesConnector;
	}

	@XmlElement(name = "typeOfChargerConnector", nillable = true)
	public String getTypeOfChargerConnector() {
		return typeOfChargerConnector;
	}

	public void setTypeOfChargerConnector(String typeOfChargerConnector) {
		this.typeOfChargerConnector = typeOfChargerConnector;
	}
}
