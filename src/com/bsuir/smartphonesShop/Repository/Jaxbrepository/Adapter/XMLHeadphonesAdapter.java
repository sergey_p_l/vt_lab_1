package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.Headphones;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLHeadphones;

public class XMLHeadphonesAdapter {

	public static XMLHeadphones toXML(Headphones headphones) {
		XMLHeadphones xmlHeadphones = new XMLHeadphones();
		xmlHeadphones.setPrice(headphones.getPrice());
		xmlHeadphones.setType(headphones.getType());
		xmlHeadphones.setTypeOfConnector(headphones.getTypeOfConnector());
		xmlHeadphones.setManufacturer(XMLManufacturerAdapter.toXML(headphones.getManufacturer()));
		return xmlHeadphones;
	}

	public static Headphones toHeadphones(XMLHeadphones xmlHeadphones) {
		Headphones headphones = new Headphones();
		headphones.setPrice(xmlHeadphones.getPrice());
		headphones.setType(xmlHeadphones.getType());
		headphones.setTypeOfConnector(xmlHeadphones.getTypeOfConnector());
		headphones.setManufacturer(XMLManufacturerAdapter.toManufacturer(xmlHeadphones.getManufacturer()));
		return headphones;
	}
}
