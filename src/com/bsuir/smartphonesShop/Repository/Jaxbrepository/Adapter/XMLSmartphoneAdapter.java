package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.Smartphone;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLSmartphone;

public class XMLSmartphoneAdapter {

	public static XMLSmartphone toXML(Smartphone smartphone) {
		XMLSmartphone xmlSmartphone = new XMLSmartphone();
		xmlSmartphone.setPrice(smartphone.getPrice());
		xmlSmartphone.setFullName(smartphone.getFullName());
		xmlSmartphone.setSmartphoneOS(smartphone.getSmartphoneOS());
		xmlSmartphone.setBatteryVolume(smartphone.getBatteryVolume());
		xmlSmartphone.setVersionOS(smartphone.getVersionOS());
		xmlSmartphone.setManufacturer(XMLManufacturerAdapter.toXML(smartphone.getManufacturer()));
		xmlSmartphone.setTypeOfChargerConnector(smartphone.getTypeOfChargerConnector());
		xmlSmartphone.setTypeOfHeadphonesConnector(smartphone.getTypeOfHeadphonesConnector());
		return xmlSmartphone;
	}

	public static Smartphone toSmartphone(XMLSmartphone xmlSmartphone) {
		Smartphone smartphone = new Smartphone();
		smartphone.setPrice(xmlSmartphone.getPrice());
		smartphone.setFullName(xmlSmartphone.getFullName());
		smartphone.setSmartphoneOS(xmlSmartphone.getSmartphoneOS());
		smartphone.setBatteryVolume(xmlSmartphone.getBatteryVolume());
		smartphone.setVersionOS(xmlSmartphone.getVersionOS());
		smartphone.setManufacturer(XMLManufacturerAdapter.toManufacturer(xmlSmartphone.getManufacturer()));
		smartphone.setTypeOfChargerConnector(xmlSmartphone.getTypeOfChargerConnector());
		smartphone.setTypeOfHeadphonesConnector(xmlSmartphone.getTypeOfHeadphonesConnector());
		return smartphone;
	}
}
