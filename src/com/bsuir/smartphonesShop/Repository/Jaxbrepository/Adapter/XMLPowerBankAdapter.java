package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.PowerBank;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLPowerBank;

public class XMLPowerBankAdapter {

	public static XMLPowerBank toXML(PowerBank powerBank) {
		XMLPowerBank xmlPowerBank = new XMLPowerBank();
		xmlPowerBank.setPrice(powerBank.getPrice());
		xmlPowerBank.setCountConnections(powerBank.getCountConnections());
		xmlPowerBank.setVolumePower(powerBank.getVolumePower());
		xmlPowerBank.setManufacturer(XMLManufacturerAdapter.toXML(powerBank.getManufacturer()));
		return xmlPowerBank;
	}

	public static PowerBank toPowerBank(XMLPowerBank xmlPowerBank) {
		PowerBank powerBank = new PowerBank();
		powerBank.setPrice(xmlPowerBank.getPrice());
		powerBank.setCountConnections(xmlPowerBank.getCountConnections());
		powerBank.setVolumePower(xmlPowerBank.getVolumePower());
		powerBank.setManufacturer(XMLManufacturerAdapter.toManufacturer(xmlPowerBank.getManufacturer()));
		return powerBank;
	}
}
