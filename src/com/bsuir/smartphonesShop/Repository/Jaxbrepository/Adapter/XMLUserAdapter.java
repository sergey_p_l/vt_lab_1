package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.User;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLUser;

public class XMLUserAdapter {

	public static XMLUser toXML(User user) {
		XMLUser xmlUser = new XMLUser();
		xmlUser.setLogin(user.getLogin());
		xmlUser.setName(user.getName());
		xmlUser.setEmail(user.getEmail());
		xmlUser.setSurname(user.getSurname());
		xmlUser.setPassword(user.getPassword());
		xmlUser.setIsAdmin(user.getIsAdmin());
		return xmlUser;
	}

	public static User toUser(XMLUser xmlUser) {
		User user = new User();
		user.setLogin(xmlUser.getLogin());
		user.setName(xmlUser.getName());
		user.setEmail(xmlUser.getEmail());
		user.setSurname(xmlUser.getSurname());
		user.setPassword(xmlUser.getPassword());
		user.setIsAdmin(xmlUser.getIsAdmin());
		return user;
	}
}
