package com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter;

import com.bsuir.smartphonesShop.DAL.Entity.Manufacturer;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.XMLManufacturer;

public class XMLManufacturerAdapter {

	public static XMLManufacturer toXML(Manufacturer manufacturer) {
		XMLManufacturer xmlManufacturer = new XMLManufacturer();
		xmlManufacturer.setCountry(manufacturer.getCountry());
		xmlManufacturer.setFullName(manufacturer.getFullName());
		return xmlManufacturer;
	}

	public static Manufacturer toManufacturer(XMLManufacturer xmlManufacturer) {
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setCountry(xmlManufacturer.getCountry());
		manufacturer.setFullName(xmlManufacturer.getFullName());
		return manufacturer;
	}
}
