package com.bsuir.smartphonesShop.Repository.Jaxbrepository;

import com.bsuir.smartphonesShop.DAL.Entity.*;
import com.bsuir.smartphonesShop.Repository.DAO;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.Entity.*;
import com.bsuir.smartphonesShop.Repository.Jaxbrepository.Adapter.*;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class XMLDAO<T> implements DAO<T> {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	private File file;
	private File validationFile;

	public XMLDAO(File file, File validationFile) {
		this.file = file;
		this.validationFile = validationFile;
		XMLConfigurationManager configurationManager = XMLConfigurationManager.getInstance();
		marshaller = configurationManager.getMarshaller();
		unmarshaller = configurationManager.getUnmarshaller(validationFile);
	}

	public XMLDAO(File file) {
		this.file = file;
		XMLConfigurationManager configurationManager = XMLConfigurationManager.getInstance();
		marshaller = configurationManager.getMarshaller();
		unmarshaller = configurationManager.getUnmarshaller();
	}

	@Override
	public void save(Iterable<T> objects) {
		try {
			XMLObjectsList list = new XMLObjectsList();
			list.setList(StreamSupport.stream(objects.spliterator(), false)
					.map(this::convertToXml)
					.collect(Collectors.toList()));
			marshaller.marshal(list, file);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Iterable<T> read() {
		try {
			XMLObjectsList list = (XMLObjectsList) unmarshaller.unmarshal(file);
			return (Iterable<T>) list
					.getList()
					.stream()
					.map(this::convertToObjects)
					.collect(Collectors.toList());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Object convertToXml(Object o) {
		if (o instanceof User) {
			return XMLUserAdapter.toXML((User) o);
		} else if (o instanceof Charger) {
			return XMLChargerAdapter.toXml((Charger) o);
		} else if (o instanceof Headphones) {
			return XMLHeadphonesAdapter.toXML((Headphones) o);
		} else if (o instanceof Manufacturer) {
			return XMLManufacturerAdapter.toXML((Manufacturer) o);
		} else if (o instanceof PowerBank) {
			return XMLPowerBankAdapter.toXML((PowerBank) o);
		} else if (o instanceof Smartphone) {
			return XMLSmartphoneAdapter.toXML((Smartphone) o);
		}
		return o;
	}

	private T convertToObjects(Object o) {
		try {
			if (o instanceof XMLUser) {
				return (T) XMLUserAdapter.toUser((XMLUser) o);
			} else if (o instanceof XMLCharger) {
				return (T) XMLChargerAdapter.toCharger((XMLCharger) o);
			} else if (o instanceof XMLHeadphones) {
				return (T) XMLHeadphonesAdapter.toHeadphones((XMLHeadphones) o);
			} else if (o instanceof XMLManufacturer) {
				return (T) XMLManufacturerAdapter.toManufacturer((XMLManufacturer) o);
			} else if (o instanceof XMLPowerBank) {
				return (T) XMLPowerBankAdapter.toPowerBank((XMLPowerBank) o);
			} else if (o instanceof XMLSmartphone) {
				return (T) XMLSmartphoneAdapter.toSmartphone((XMLSmartphone) o);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (T) o;
	}
}
