package com.bsuir.smartphonesShop;

import com.bsuir.smartphonesShop.Controller.Controller;
import com.bsuir.smartphonesShop.Controller.command.Exception.CommandException;
import com.bsuir.smartphonesShop.DAL.Entity.User;

import java.util.*;

import static com.bsuir.smartphonesShop.View.ChargerView.showListChargers;
import static com.bsuir.smartphonesShop.View.HeadphonesView.showListHeadphones;
import static com.bsuir.smartphonesShop.View.ManufacturerView.showListManufacturers;
import static com.bsuir.smartphonesShop.View.PowerBankView.showListPowerBanks;
import static com.bsuir.smartphonesShop.View.SmartphoneView.showListSmartphones;
import static com.bsuir.smartphonesShop.View.UserView.enterAccount;

public class Main {
    private static Scanner scn = new Scanner(System.in);
    private static Controller controller = new Controller();

    public static void main(String[] args) {
        try {
            String response = controller.execute("load");
            System.out.println(response);
        } catch (CommandException e) {
            System.out.println(e.getMessage());
        }
        User user = enterAccount();
        if (user == null)
            return;
        while (true) {
            System.out.println("######################################");
            System.out.println("#      Please choice the action      #");
            System.out.println("######################################");
            System.out.println("# 1 - Select Chargers");
            System.out.println("# 2 - Select Headphones");
            System.out.println("# 3 - Select Manufacturers");
            System.out.println("# 4 - Select PowerBanks");
            System.out.println("# 5 - Select Smartphones");
            System.out.println("# 0 - Exit");
            System.out.println("######################################");
            String action = scn.nextLine();
            switch (action) {
                case ("1"):
                    showListChargers();
                    break;
                case ("2"):
                    showListHeadphones();
                    break;
                case ("3"):
                    showListManufacturers();
                    break;
                case ("4"):
                    showListPowerBanks();
                    break;
                case ("5"):
                    showListSmartphones();
                    break;
                case ("0"):
                    return;
            }
        }
    }
}